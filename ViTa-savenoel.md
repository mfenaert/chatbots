---
style: h1{font-family:Papyrus, sans-serif;color:#347082;text-decoration:bold}li{font-family:Verdana, "DejaVu Sans", sans-serif}p{font-family:Verdana, "DejaVu Sans", sans-serif}footer{font-family:Papyrus, sans-serif}aside{font-size:0.5em}audio{visibility:hidden}main{background:linear-gradient(90deg, rgb(239,247,242) 0%, rgb(237,249,240) 15.2057%, rgb(235,248,239) 26.297%, rgb(235,248,239) 28.9803%, rgb(231,249,237) 47.585%, rgb(230,250,236) 48.6583%, rgb(228,249,236) 57.6029%, rgb(227,250,234) 65.4741%, rgb(222,250,234) 72.093%, rgb(219,248,230) 79.4275%, rgb(216,248,229) 86.2254%, rgb(213,249,228) 89.4454%, rgb(210,249,226) 94.2755%, rgb(209,248,225) 96.6011%, rgb(208,247,224) 100%)}.bot-message ul.messageOptions li>a:hover{background-color:#B6CE8A;border-color:#43481D}.user-message{background-color:#447C25}
contenuDynamique: true
messageParDéfaut: [Hmmm… je crois que je n’ai pas tout saisi. Fais une autre proposition.]
variables:
    repondre: "C'est à toi de répondre ! /// À ton tour ! /// Sauras-tu me répondre... ? /// Es-tu au point ? /// As-tu bien révisé ?"
    def-eco: "métabolisme cellulaire /// autotrophe /// hétérotrophe /// photosynthèse /// respiration cellulaire /// écosystème /// agrosystème /// matière organique /// matière minérale /// producteur primaire /// producteur secondaire /// décomposeur /// biome /// succession écologique /// climax /// résilience /// relations trophiques /// parasitisme /// symbiose /// compétition /// commensalisme /// mutualisme /// biotope /// biocénose /// facteurs abiotiques /// facteurs biotiques /// espèce climacique /// héliophile /// épiphyte /// saprophyte /// sciaphile /// symbiote /// hygrophile /// plante annuelle /// plante vivace /// aérophile /// parasite /// écologie /// biomasse /// cycle biogéochimique "
    bravo: "Bravo ! /// Tout à fait ! /// Bonne réponse ! /// C'est exact ! /// C'est une bonne réponse ! /// Excellent !"
    erreur: "C'est une erreur :-1:/// Eh non :anguished:/// Pas de chance :laughing:/// C'est inexact :anguished: /// Ce n'est pas la bonne réponse :cry: /// Aïe aïe aïe !:laughing:/// Nope... "
    encourage: " Cherche encore. /// Recommence. /// Essaye encore ! /// Retente ta chance ! /// Réfléchis encore. /// Propose autre chose !"
avatar: https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa_-_rond.png
favicon: https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa_-_rond2.png
clavier: false
tags: chatbot, ViTa, SVT, 2de, 1èreSpé
obfuscate: true
gestionGrosMots: true
rechercheContenu: true
footer: CC BY NC SA Mélanie Fenaert 2024 - Créé avec ChatMD, un outil libre et gratuit de Cédric Eyssette
useLLM:
    url: https://api.cohere.com/v1/chat
    askAPIkey: false
    encryptedAPIkey: KzxnYTwOE2s1MTo3C2cCEzkeHRk/Iz0bIDxhKncTMTEEZwBifS0VMA==
    informations: "https://codimd.apps.education.fr/H5ALGWxfQYai0c2T9FZb6Q/download"
    separator: auto
    postprompt: "Réponds en français, en 10 phrases maximum. Utilise le gras, l'italique et les listes en Markdown pour mettre en valeur les éléments importants. Termine sur une affirmation, pas de question."
    model: command-r-plus
    maxTopElements: 5
    
---

# ViTa

Bonjour, je suis **ViTa** !

![Avatar de ViTa : pixel art d'une enseignante avec une blouse et un stylo devant un tableau à craie et un microscope](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa_-_rond.png "Image créée avec Copilot Designer")

Je suis ton assistante de révision en Sciences de la vie et de la Terre.

Je suis spécialisée dans les niveaux 2de et 1ère spécialité SVT.

**Choisis un thème** ci-dessous, pour répondre à des questions dans le niveau voulu. 

Si tu as mis le mot de passe (ou la clé API), pour avoir plus d'explications sur certaines notions grâce à l'IA, tu pourras parfois choisir d'**En savoir plus**, mais garde un regard critique sur mes réponses ! :wink: 

Quel sujet souhaites-tu réviser ? 

1. [La biodiversité et son évolution](Biodiversité) 
2. [Le corps humain et la santé](Santé) 
3. [La génétique](Génétique)
4. [La planète Terre](Terre)
5. [Réviser plutôt les méthodes](Méthodo)


## test
`@KEYBOARD = true`
"Les oiseaux ont des plumes pour pouvoir voler."

Reformule cette phrase pour qu'elle ne soit pas finaliste.

`@reponseplumes = @INPUT : plumes`


## plumes

*Réponse générée par IA, garder l'esprit critique :*

`!useLLM`

J'ai demandé à un élève de reformuler la phrase "Les oiseaux ont des plumes pour pouvoir voler." pour qu'elle ne soit pas finaliste.

Voici sa réponse : `@reponseplumes`.

Évalue sa réponse : si cela reste finaliste dis le clairement. Favorise les reformulations correctes s'appuyant sur l'évolution, par exemple “Les plumes d’un oiseau facilitent le vol.” ou "Les plumes des oiseaux procurent un avantage sélectif en favorisant le vol.". Donne lui des conseils pour s'améliorer.  Adresse-toi à lui directement en le tutoyant.

`END !useLLM`

*Fin de la réponse générée par l'IA.*

texte etc.

## Méthodo

Nous allons revoir quelques éléments de méthodologie, importants en SVT. 
Sur quel sujet sujet souhaites-tu t'entraîner ?

1. [La question de synthèse](Synthèse)
2. [L'argumentation à partir de documents](Argumentation)
3. [Comment rédiger rigoureusement ?](Rédiger)


## Synthèse

La question de synthèse est l'exercice 1 de l'épreuve du Bac.
Il est évalué sur **6 ou 7 points**.
Généralement il n'y a aucun document, parfois un document peut être fourni pour aider ou orienter la réponse.

Quoiqu'il en soit, le meilleur moyen de réussir cet exercice est de **connaître et comprendre parfaitement son cours** !
Mais cela n'est pas suffisant pour complètement réussir l'exercice : il faut aussi respecter la démarche attendue.

1. [Réviser la démarche de la question de synthèse](Synth1)

## Synth1

Comment doit être structurée l'introduction ?
1. [uniquement réécrire le sujet](Erreur)
2. [amener le sujet en définissant les mots clés](Synth1b)
3. [reformuler le sujet sous forme de question](Synth1b)
4. [annoncer le plan](Synth1b)

## Synth1b
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Il y avait plusieurs bonnes réponses, les as-tu toutes trouvées ?

Si non, propose une autre réponse.
Si oui, passe à la suite.

1) [Passer à la suite](Synth2)

## Synth2

L'introduction est composée de : 
* un paragraphe **amenant progressivement le sujet** tout en définissant les **mots clés**
* un problème sous forme de **question** qui reformule le sujet
* une **annonce de plan** : un développement en 2 ou 3 parties

Concernant le développement...
1) [il est obligatoire de mettre des titres numérotés](Erreur)
2) [les parties doivent être équilibrées en longueur](Synth2b)
3) [il vaut mieux intercaler les schémas et le texte](Synth2b)
4) [les schémas ne sont pas importants](Erreur)

## Synth2b
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Il y avait plusieurs bonnes réponses, les as-tu toutes trouvées ?

Si non, propose une autre réponse.
Si oui, passe à la suite.

1) [Passer à la suite](Synth3)

## Synth3

**Les titres des paragraphes ne sont pas obligatoires**.
Ils sont intéressants pour montrer l'organisation du développement, mais si tu passes des lignes et fais de bonnes transitions cela peut suffire.

**Les parties doivent être équilibrées.** 
Par exemple si tu veux faire 3 parties mais qu'une est finalement très courte, il vaut mieux la fusionner avec une autre pour faire 2 parties d'à peu près même longueur.

**Les schémas sont très importants !** 
Ils permettent de décrire de nombreux phénomènes biologiques et géologiques. Ils doivent être **grands**, **titrés** et **légendés** de manière complète. Les **couleurs** permettent de distinguer les différents éléments du schéma. Il vaut mieux les **intercaler entre les paragraphes** plutôt que tous les mettre en fin d'une partie.

Comment rédiger la conclusion ?

1) [il faut réécrire tout le développement](Erreur)
2) [il faut synthétiser les idées importantes](Synth3b)
3) [la conclusion doit répondre à la question posée en introduction](Synth3b)
4) [on peut ouvrir le sujet vers une question connexe](Synth3b)

## Synth3b
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Il y avait plusieurs bonnes réponses, les as-tu toutes trouvées ?

Si non, propose une autre réponse.
Si oui, passe à la suite.

1) [Passer à la suite](Synth4)

## Synth4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

La conclusion doit **répondre au problème posé** en introduction en faisant une **courte synthèse** de l'exposé et **ouvrir** sur un autre problème.

Un schéma bilan peut être associé à la conclusion, cela dépend des sujets.


1. [Voir une vidéo sur la question de synthèse](SynthVid)
2. [Revenir à la méthodologie](Méthodo)
3. [Revenir au début]()

## SynthVid

Cette vidéo résume tous les éléments vus précédemment.
Contrairement à ce qu'il est indiqué au début, désormais cet exercice est noté sur 6 ou 7 points.

<iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=IoZAkOVAWhU&vignette=https://i.ytimg.com/vi/IoZAkOVAWhU/hqdefault.jpg&debut=0&fin=445&largeur=16&hauteur=9" allow="autoplay; fullscreen" frameborder="0" width="700" height="394"></iframe>

2. [Revenir à la méthodologie](Méthodo)
3. [Revenir au début]()


## Argumentation

C'est l'exercice 2 de l'épreuve écrite du Bac.
Il est évalué sur **8 ou 9 points**.

Cette partie de l'épreuve valide la **pratique du raisonnement scientifique et de l’argumentation**, et porte sur 1 ou 2 parties du programme, pas nécessairement différentes de l'exercice 1.

Il s’appuie sur l’**exploitation d’un ensemble de documents** (dossier documentaire).

1. [Réviser la démarche de l'argumentation](Argu1)

## Argu1

Comment doit être structurée l'introduction ?
1. [c'est un piège ! il n'y a pas besoin d'introduction](Erreur)
2. [amener le sujet en définissant les mots clés](Erreur)
3. [reformuler le sujet sous forme de question](Argu2)
4. [annoncer le plan](Erreur)

## Argu2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

L'introduction est nécessaire mais, contrairement à la question de synthèse, elle doit être courte : une simple **reformulation du problème** suffit, sous forme de **question**.


Concernant les documents...
1) [il faut les étudier dans l'ordre](Erreur)
2) [ils ont tous la même importance pour l'argumentation](Erreur)
3) [il n'y a pas besoin de les présenter](Erreur)
4) [il faut les mettre en relation entre eux et avec les connaissances](Argu3)

## Argu3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

C'est un ensemble de documents qu'il faut étudier, il faut donc le faire en **recherchant des relations entre eux**, en **lien avec le problème posé**. 

Tes **connaissances** peuvent éclairer certains éléments issus des documents.

Certains documents sont illustratifs, d'autres sont à explorer plus profondément (résultats d'expériences, données sous forme de tableaux ou graphiques, etc.). Mais **tous doivent être cités à un moment donné**, il n'y a pas de document piège !

Concernant le développement...
1) [il est obligatoire de mettre des titres numérotés](Erreur)
2) [il faut commencer par présenter tous les documents](Erreur doc)
3) [il faut faire un paragraphe par document](Erreur)
4) [il faut faire un paragraphe par argument](Argu4)
5) [les schémas ne sont pas importants](Erreur)

## Erreur doc
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav

C'est une erreur à ne surtout pas faire ! :no_entry: 

On présente un document **au moment où on en a besoin** dans l'argumentation.

@{encourage}

## Argu4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Dans une argumentation, il faut en effet rédiger **un paragraphe par argument**. 

**Les titres ne sont pas nécessaires**, passe des lignes entre chaque argument, fais des transitions. 

Les **schémas ne sont pas obligatoires** mais ils peuvent aider à **préciser** tes écrits.

Tu dois présenter la démarche choisie pour répondre à la problématique, dans un texte **soigné**, **cohérent**, et mettant clairement en évidence les **relations** entre les divers documents et arguments utilisés. 

Mais au fait, c'est quoi un argument ?
1) [c'est un document](Erreur)
2) [c'est un ensemble de connaissances](Erreur)
3) [c'est un paragraphe toujours structuré par "j'observe que.. or je sais que... donc j'en déduis que..."](Erreur parag)
4) [c'est une idée reposant sur l'étude d'un ou plusieurs documents, éventuellement éclairée par des connaissances](Argu5)

## Erreur parag
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
@{erreur}

**Cette syntaxe est une recommandation**, elle peut t'aider à structurer ton raisonnement en partant des observations vers les déductions.
Mais **elle n'est pas rigide**.

Par exemple, *il n'y a pas toujours de connaissance* à insérer avec un "or je sais que...".

Autre exemple, *on peut faire plusieurs observations issues de plusieurs documents*, et seulement ensuite en déduire quelque chose.

@{encourage}

## Argu5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

C'est en effet la définition correcte d'un argument !

Ainsi, dans un paragraphe, on construit un argument reposant sur **l'étude d'un ou plusieurs documents**, éventuellement **éclairée par une ou des connaissances**.

Les **connecteurs logiques** sont importants pour articuler ton raisonnement.

Une **syntaxe est recommandée**, *sans être toutefois une obligation rigide*. 

La liste ci-dessous n'est qu'un exemple de possibles : 
* le document 1 est un… qui montre que…, **donc** on en déduit que… 
* **or** on sait que…
* le document 2, qui est un…, permet de voir que..., **ce que l’on peut mettre en relation avec** les données du document 4… **donc** on en conclut que…
* **enfin** dans le document 3, qui est un…, on observe que..., **ainsi** on en déduit que… 

Comment rédiger la conclusion ?

1) [il faut réécrire tout le développement](Erreur)
2) [il faut synthétiser les idées importantes](Argu6)
3) [la conclusion doit répondre à la question posée en introduction](Argu6)
4) [on peut ouvrir le sujet vers une question connexe](Erreur)

## Argu6
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Il y avait plusieurs bonnes réponses, les as-tu toutes trouvées ?

Si non, propose une autre réponse.
Si oui, passe à la suite.

1) [Passer à la suite](Argu6b)

## Argu6b
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

La conclusion doit **répondre au problème posé** en introduction en faisant une **courte synthèse** de l'exposé.

1. [Voir une vidéo sur l'argumentation](ArguVid)
2. [Revenir à la méthodologie](Méthodo)
3. [Revenir au début]()

## ArguVid

Cette vidéo résume tous les éléments vus précédemment.
Contrairement à ce qu'il est indiqué au début, cette partie est désormais notée sur 8 ou 9 points, et depuis quelques années elle n'est constituée que d'un seul exercice.

<iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=ckTaohUUm6U&vignette=https://i.ytimg.com/vi/ckTaohUUm6U/hqdefault.jpg&debut=0&fin=587&largeur=16&hauteur=9" allow="autoplay; fullscreen" frameborder="0" width="700" height="394"></iframe>

2. [Revenir à la méthodologie](Méthodo)
3. [Revenir au début]()




## Rédiger

En sciences, on insiste beaucoup sur **la rigueur de la rédaction**.

Mais qu'est-ce que cela signifie ?

1. [Démarrer le quiz](Rédi1)

## Rédi1
Pour rédiger avec rigueur en sciences, quelle est la meilleure façon de présenter des informations ?

1) [Utiliser des phrases complexes et un vocabulaire difficile pour montrer sa connaissance](Erreur)  
2) [Présenter des idées de manière structurée, avec des termes précis et clairs](Rédi2)  
3) [Utiliser des opinions personnelles pour enrichir le texte](Erreur)  
4) [Employer des comparaisons fréquentes pour simplifier tous les concepts](Erreur)  

## Rédi2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Présenter des idées de manière **structurée**, avec des termes **précis et clairs**, est une marque de rigueur.

Comment structurer efficacement un texte scientifique ?

1) [Présenter les informations de manière progressive, en suivant un ordre logique](Rédi3)
2) [Mettre les informations importantes en premier, puis ajouter des détails aléatoirement](Erreur)
3) [Commencer par les conclusions, puis justifier par des idées diverses](Erreur)
4) [Inclure des informations sans ordre précis pour que le lecteur puisse les interpréter librement](Erreur)

## Rédi3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Présenter les informations de manière progressive, en suivant un ordre logique, est un mode de rédaction rigoureux.

Pour marquer cette structuration, il faut utiliser des **connecteurs logiques**. Ceux à préférer en sciences sont ceux qui montrent un raisonnement **de cause à effet**.

Parmi cette liste, lesquels sont à favoriser dans ta rédaction ?
1) [en effet](Erreur)
2) [donc](Rédi3b)
3) [car](Erreur)
4) [parce que](Erreur)
5) [d'abord](Rédi3b)
6) [puis](Rédi3b)
7) [ainsi](Rédi3b)
8) [enfin](Rédi3b)
9) [à cause de](Erreur)

## Rédi3b
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Il y avait plusieurs bonnes réponses, les as-tu toutes trouvées ?

Si non, propose une autre réponse.
Si oui, passe à la suite.

1) [Passer à la suite](Rédi4)

## Rédi4
`@KEYBOARD = true`
**Donc, ainsi, d'abord, puis, enfin**... marquent un ordre logique, chronologique ou de cause à effet.

*En effet, car, parce que, à cause de*... sont à **éviter** : ils marquent un raisonnement "à l'envers".

Il y a d'autres connecteurs à éviter, notamment : "afin de"
Pourrais-tu expliquer ce qui n'est pas rigoureux dans cette phrase ?

*La cellule réplique son ADN afin de se diviser lors de la mitose.*

`@reponseafin = @INPUT : afin`


## afin
`@KEYBOARD = true`
Écris "continue" pour passer à la suite.

*Réponse générée par IA, garder l'esprit critique :*

`!useLLM`

J'ai demandé à un élève d'expliquer ce qui n'est pas rigoureux dans cette phrase : "La cellule réplique son ADN afin de se diviser lors de la mitose."

Voici sa réponse : `@reponseafin`.

Évalue sa réponse : l'élève doit expliquer que l'expression afin traduit un but, un objectif, qu'on ne peut appliquer à une cellule. Explique lui que cette phrase est finaliste, explique lui ce qu'est le finalisme. Adresse-toi à lui directement en le tutoyant.

`END !useLLM`

*Fin de la réponse générée par l'IA.*

1) [Passer à la suite](Rédi5)

!Next: Roche Rédi5

## Rédi5
- continue

Le connecteur "afin de" est une marque de **finalisme**. 

Voici une définition de l'académie française, pour le domaine de la biologie : **doctrine selon laquelle les processus vitaux manifestent une intention, semblent dirigés vers un but**.

En biologie, le finalisme a longtemps influencé la manière de concevoir les organismes, en pensant que chaque organe ou structure d’un être vivant avait été conçu *pour remplir une fonction spécifique*. 

Par exemple, on pourrait dire que l’œil existe “pour voir”, ou que les plumes d’un oiseau existent “pour voler”. Elles impliquent une intention dans la nature. 

Au lieu de cela, les scientifiques **préfèrent utiliser des explications basées sur les processus**, souvent la sélection naturelle.

Revenons à notre phrase : *La cellule réplique son ADN afin de se diviser lors de la mitose.*

Quelle reformulation serait la plus rigoureuse et non finaliste ?

1) [La cellule réplique son ADN pour préparer sa division lors de la mitose.](Rédi5d)
2) [La cellule réplique son ADN, ce qui lui permet de se diviser ensuite lors de la mitose.](Rédi5b)
3) [La réplication de l'ADN double la quantité d'ADN, permettant à la cellule de disposer de deux copies de son génome avant la mitose.](Rédi5c)

## Rédi5d
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
@{erreur}

Tu as remplacé "afin de" par "pour", mais cela revient au même, il y a toujours une idée de but, d'objectif : cette phrase est tout de même finaliste.

@{encourage}

## Rédi5b

Assez bonne proposition, c'est une phrase un peu moins finaliste. L'expression "ce qui lui permet de" implique moins un but qu'aurait la cellule.

Il y a une autre proposition plus rigoureuse. Cherche encore.

## Rédi5c
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

C'est en effet la meilleure proposition, la moins finaliste.

Cela demande souvent un peu plus d'explications, mais le phénomène est décrit bien plus rigoureusement ! :wink: 

Allez, un dernier test : 

*Les oiseaux ont des plumes pour voler* : choisis la reformulation la moins finaliste.

1) [Les oiseaux ont des plumes dans le but de voler.](Erreur)
2) [Les oiseaux ont développé des plumes qui leur permettent de voler.](Erreur)
3) [Les plumes des oiseaux facilitent le vol, ce qui est un avantage conservé lors de l'évolution.](Rédi6)

## Rédi6
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav
@{bravo}

Cette proposition n'implique pas de but et repose sur une explication évolutive correctement formulée.

2. [Revenir à la méthodologie](Méthodo)
3. [Revenir au début]()

## Terre

Cette partie s'intéresse à notre planète du point de vue géologique : sa structure interne, les manifestations en surface de son activité interne, les phénomènes externes comme l'érosion, la formation des sols...

Choisis ton niveau.

1. [Niveau collège-2de](Terre 2de)
2. [Niveau 1ère spécialité SVT](Terre 1spé)
3. [Retour au message initial]()

## Terre 2de

Dans ce quiz nous allons revoir des notions de collège et de 2de qui seront utiles pour la classe de 1ère.

1. [Démarrer le quiz](Terre q1)

## Terre q1

Notre planète est une planète rocheuse. Elle est constituée de plusieurs couches concentriques.

La couche la plus externe de la Terre est :
1) [la croûte terrestre](Terre q1bis)
2) [le manteau](Erreur)
3) [le noyau externe](Erreur)
4) [le noyau interne](Erreur)

## Terre q1bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Voici un schéma simplifié de la structure interne de la Terre.

![Structure interne Terre niveau collège](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/structureterre-college.PNG "structure de la Terre en couches concentriques")
<aside>Source : SVT Dijon modifié</aside>

1. [Question suivante](Terre q2)

## Terre q2

Le manteau terrestre est principalement composé de :
1) [roches solides](Terre q3)
2) [glace](Erreur)
3) [métaux liquides](Erreur)
4) [magma](Erreur)


## Terre q3
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Le manteau est bien constitué de **roches solides**. 

1. [En savoir plus](!useLLM Explique quelle roche constitue le manteau terrestre et pourquoi elle reste solide dans le manteau.)
2. [Question suivante](Terre q3bis)

## Terre q3bis

Au-delà de la distinction croûte/manteau/noyau, on parle aussi de la lithosphère. De quoi est-elle constituée ?

1) [la croûte terrestre et la partie supérieure du manteau supérieur](Terre q4)
2) [la croûte terrestre et la totalité du manteau](Erreur)
3) [le manteau inférieur et le noyau externe](Erreur)
4) [le noyau externe et le noyau interne](Erreur)


## Terre q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

La **lithosphère** est bien constituée de la croûte terrestre associée à la partie supérieure du manteau supérieur (le manteau lithosphérique).

La lithosphère terrestre est découpée en grandes plaques.

![Plaques tectoniques](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/1024px-Tectonic_plates-fr.png)
<aside>Source : USGS, Public domain, via Wikimedia Commons</aside>

Une **plaque lithosphérique ou tectonique** est une zone stable de la surface de la Terre délimitée par des zones de forte activité géologique.

Les plaques tectoniques sont en mouvement à la surface de la Terre. Elles se déplacent sur :
1) [l'asthénosphère](Terre q4bis)
2) [la croûte terrestre](Erreur)
3) [le noyau externe](Erreur)
4) [la lithosphère](Erreur)

## Terre q4bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Sous la lithosphère, bien que solide, le manteau supérieur a une rigidité moindre, une plus grande capacité de déformation : on parle d'**asthénosphère** ou de manteau asthénosphérique.

2. [Question suivante](Terre q5)

## Terre q5

La limite où deux plaques tectoniques se rencontrent est appelée une frontière de plaques, ou encore une marge active. Selon les frontières, on peut y observer :
1) [un important volcanisme](volcanisme)
2) [de nombreux tremblements de terre](tremblements)
3) [des failles](failles)
4) [de profonds fossés océaniques](fosses)

## volcanisme
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Les frontières de plaques sont en effet marquées par un important volcanisme, continental comme par exemple dans la Cordillère des Andes, ou sous-marin comme au milieu de l'Atlantique.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Terre q5bis)

## tremblements
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Les frontières de plaque se caractérisent par une forte activité sismique.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Terre q(q5bis))

## failles
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Les failles de différents types (normales, inverses, décrochantes, transformantes) sont présentes au niveau des frontières de plaques.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Terre q5bis)

## fosses
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Dans certaines zones, les limites de plaques sont en effet bordées de profondes fosses océaniques : c'est par exemple le cas tout autour de l'océan Pacifique.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Terre q5bis)

## Terre q5bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

Toutes les propositions étaient correctes ! 

Aux frontières de plaques, on observe une importante **activité sismique et volcanique**, mais aussi des **failles** et selon la zone des **fosses océaniques**.

Tu as atteint le niveau 1.

1. [Passer au niveau 2](Terre q6)
2. [Retour au message initial]()

## Terre q6
L'activité interne de la Terre façonne depuis des milliards d'années les reliefs de notre planète.

Mais il existe aussi des phénomènes se produisant uniquement à la surface, comme l'érosion.

L'érosion est le processus par lequel :
1) [les roches et le sol sont usés par l'eau, le vent et la glace](Terre q6bis)
2) [les montagnes se forment](Erreur)
3) [les volcans émettent de la lave](Erreur)
4) [les plaques tectoniques se déplacent](Erreur)

## Terre q6bis
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

L’**érosion** est le processus de dégradation et de transformation du relief, et donc des sols, roches, berges et littoraux qui est causé **par tout agent externe** (donc autre que la tectonique) tel que l'eau, le vent, la glace.

1. [En savoir plus](!useLLM Donne des exemples d'actions de l'érosion à différentes échelles de temps : minute, heures, années, millions d'années.)
2. [Question suivante](Terre q7)

## Terre q7

Quelle est la composition d'un sol ?
1) [minéraux, matière organique, air et eau](Terre q8)
2) [roches solides uniquement](Erreur)
3) [eau et minéraux uniquement](Erreur)
4) [gaz uniquement](Erreur)

## Terre q8
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Un sol comporte de la matière minérale, de la matière organique, de l'eau et de l'air. 

Il est structuré en différentes couches nommées horizons. Voici par exemple la structure d'un sol forestier : 
![Structure d'un sol](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/sol.PNG)
<aside>Source : SVT Dijon</aside>


La matière organique dans le sol provient principalement :
1) [de la décomposition des plantes et des animaux](Terre q8bis)
2) [de la décomposition des roches](Erreur)
3) [de l'érosion des roches par le vent, l'eau et la glace](Erreur)
4) [des pluies acides](Erreur)

## Terre q8bis
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Les matières organiques des sols forment un ensemble hétérogène constitué de débris végétaux et animaux en cours de décomposition.  

La fraction organique du sol représente un faible pourcentage, de 1% à quelques % dans les sols cultivés.


2. [Question suivante](Terre q9)

## Terre q9

Qu'est-ce qu'un agrosystème ?

1) [un écosystème modifié par l'être humain pour l'agriculture](Terre q9bis)
2) [un écosystème naturel sans intervention humaine](Erreur)
3) [un système de gestion de l'eau](Erreur)
4) [une zone de protection de la faune](Erreur)

## Terre q9bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Un agrosystème est un **écosystème cultivé** : un écosystème, terrestre ou aquatique, modifié par l’humain afin d'exploiter une part de la matière organique qu'il produit, généralement à des fins alimentaires. Ex : rizière, culture de céréales, élevage de poissons...

Mais on trouve aussi des agrosystèmes dont le but est de cultiver des produits non alimentaires, comme les biocarburants. 

1. [Question suivante](Terre q10)

## Terre q10

Quelle activité humaine  peut favoriser l'érosion et l'appauvrissement des sols ?
1) [la déforestation](Terre q10bis)
2) [la plantation d'arbres](Erreur)
3) [la construction de terrasses agricoles](Erreur)
4) [la création de zones naturelles protégées](Erreur)

## Terre q10bis

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav
@{bravo}

La **déforestation** favorise l'érosion des sols en éliminant les arbres dont les racines stabilisent et retiennent le sol. Sans cette protection, le sol devient plus vulnérable à l'action de l'eau et du vent, ce qui l'appauvrit de ses éléments fertiles.

1. [En savoir plus](!useLLM Explique comment la déforestation peut aboutir à créer des sols incultivables.)
2. [Terminer le test](Terre Fin)

## Terre Fin

Félicitations, tu as atteint le niveau 2 de ces révisions de collège et 2de sur la géologie de notre planète !

1. [Passer au niveau 1ère spécialité SVT](Terre 1spé)
2. [Retour au message initial]()


## Terre 1spé

Les travaux ont débuté... :construction_worker_woman: 

**Vos questions sélectionnées par votre professeure apparaîtront progressivement.**

Que souhaites-tu réviser ?

1. [La structure de la Terre](Struc q1)
2. [Les roches et leurs méthodes d'observation](Roche)
3. [Retour au message initial]()

## Roche

Les questions suivantes te proposent d'explorer l'univers des roches (alias les cailloux). Elles te permettront de réviser aussi les méthodes d'identification de ces roches à l'œil nu et au microscope.

1) [C'est parti !](Roche q1)

## Roche q1

À quoi ça sert d'étudier les cailloux ?

1) [à comprendre les paysages et leur histoire géologique](Roche q2)
2) [à rien](Erreur)
3) [à comprendre leurs pouvoirs magiques de guérison](Erreur litho)
4) [à faire plaisir à la prof de SVT](Erreur prof)

## Erreur litho
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
@{erreur}

On appelle ça la lithothérapie et c'est une **pseudo-science** (= le mot gentil pour dire que c'est juste n'importe quoi, et même du charlatanisme).

@{encourage}

## Erreur prof
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
C'est certain, ça lui fait plaisir, mais ce n'est pas l'objectif ! :wink:

@{encourage}


## Roche q2
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}
Quels sont les 3 grands types de roches ?

`@reponseroches = @INPUT : Roche q2bis`


## Roche q2bis

*Réponse générée par IA, garder l'esprit critique :*

`!useLLM`

J'ai demandé à un élève de me dire quels sont les 3 grands types de roches.

Voici sa réponse : `@reponseroches`.

J'attends ces trois catégories : roches sédimentaires, magmatiques et métamorphiques. Évalue sa réponse et donne lui des conseils pour l'améliorer. Adresse-toi à lui directement en le tutoyant.

`END !useLLM`

*Fin de la réponse générée par l'IA.*

1. [Question suivante](Roche q3)

## Roche q3

On s'intéresse aux roches magmatiques.
Nous en avons étudié 4 en classe.
Classe ces roches par ordre croissant de densité. 

1) [Granite, Basalte, Gabbro, Péridotite](Roche q4) 
2) [Péridotite, Gabbro, Basalte, Granite](Erreur)
3) [Granite, Gabbro, Péridotite, Basalte](Erreur)
4) [Gabbro, Basalte, Péridotite, Granite](Erreur)

## Roche q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

* Le granite est une roche continentale de densité **2,6**.
* Le basalte et le gabbro sont des roches de la croûte océanique d'une densité de **2,9**.
* La péridotite est la roche caractéristique du manteau, de densité **3,3**.

Ces valeurs sont des moyennes à retenir, les échantillons peuvent avoir des densités qui fluctuent autour de ces moyennes. 
De plus, plus on s'enfonce dans le manteau, plus les péridotites sont denses.

1. [Question suivante](Roche q4bis) 


## Roche q4bis
Voyons maintenant d'un peu plus près ces roches caractéristiques que nous avons étudiées.

Voici un exemple d'échantillon de granite décrit par Damien Jaujard, professeur agrégé de SVTU, PRAG Sciences de la Terre à l'Université Paris Est Créteil.

<iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=nTkgffMg-fc&vignette=https://i.ytimg.com/vi/nTkgffMg-fc/hqdefault.jpg&debut=14&fin=55&largeur=16&hauteur=9" allow="autoplay; fullscreen" frameborder="0" width="700" height="394"></iframe>

Voici un échantillon d'un autre granite : 

<iframe src="https://cosphilog.fr/mesurim2/?c=AAJE&full=true" name="Granite" title="Granite" style="width: 100%; height: 500px;" allowfullscreen></iframe>

Comment peut-on qualifier la structure du granite ?

1) [grenue](Roche q5) 
2) [microlitique](Erreur)
3) [dense](Erreur)
4) [vitreuse](Erreur)


## Roche q5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La structure du granite est **grenue** (ou holocristalline) : elle est entièrement cristallisée, on observe uniquement des minéraux collés les uns aux autres (on dit "jointifs").
Cette structure indique que le magma à l'origine du granite s'est **refroidi lentement en profondeur**, il a eu le temps d'entièrement cristalliser.

1. [Question suivante](Roche q5bis) 

## Roche q5bis
`@KEYBOARD = true`
Les deux roches de la croûte océanique que nous avons étudiées sont le gabbro et le basalte.

**Basalte en coussin** (plus grande largeur de l'échantillon : 42cm) :

<iframe src="https://cosphilog.fr/mesurim2/?c=AANG&full=true" name="Granite" title="Granite" style="width: 100%; height: 500px;" allowfullscreen></iframe>


**Gabbro** (LxlxH = 6x5x4cm) :

<iframe src="https://cosphilog.fr/mesurim2/?c=AAJD&full=true" name="Granite" title="Granite" style="width: 100%; height: 500px;" allowfullscreen></iframe>

D'après tes connaissances et tes observations, décris et explique les différences de structure de ces deux roches.

`@reponsebagab = @INPUT : Rocheq6`


## Rocheq6
`@KEYBOARD = true`

Écris **ROCHES** pour passer à la suite.
<br>
*Réponse générée par IA, garder l'esprit critique :*

`!useLLM`

J'ai demandé à un élève de me décrire et expliquer les différences de structure entre basalte et gabbro.

Voici sa réponse : `@reponsebagab`.

J'attends qu'il décrive le basalte comme une roche magmatique volcanique de structure microlitique, ayant refroidi rapidement en surface, située en haut de la croûte océanique ; le gabbro doit être décrit comme une roche magmatique plutonique, de structure grenue, s'étant refroidie lentement en profondeur dans la croûte océanique. Évalue sa réponse et donne lui des conseils pour l'améliorer. Adresse-toi à lui directement en le tutoyant.

`END !useLLM`

*Fin de la réponse générée par l'IA.*


!Next: Roche q6ter


## Roche q6ter
- ROCHES

Pour en savoir plus sur l'origine de ces deux roches, voici deux vidéos.

**Basalte**
Damien Jaujard, professeur agrégé de SVTU, PRAG Sciences de la Terre à l'Université Paris Est Créteil.
<iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=xyNl2WowNVY&vignette=https://i.ytimg.com/vi/xyNl2WowNVY/hqdefault.jpg&debut=0&fin=228&largeur=16&hauteur=9" allow="autoplay; fullscreen" frameborder="0" width="700" height="394"></iframe>


**Gabbro**
Robin Duborget, professeur agrégé de SVTU
<iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=_zYzuWdgiA4&vignette=https://i.ytimg.com/vi/_zYzuWdgiA4/hqdefault.jpg&debut=0&fin=110&largeur=16&hauteur=9" allow="autoplay; fullscreen" frameborder="0" width="700" height="394"></iframe>

1. [Passer à la suite](Roche q6bis)

## Roche q6bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

Tu as atteint le niveau 1 des révisions sur les roches !

**On s'arrête là pour l'instant !**

3. [Retour au message initial]()


## Struc q1

De quelle façon les altitudes terrestres sont-elles réparties ? 

1) [bimodale](Struc q2)
2) [unimodale](Erreur)
3) [aléatoire](Erreur)
4) [homogène](Erreur)


## Struc q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

En effet, le relevé des altitudes et profondeurs met en évidence une **répartition autour de deux moyennes** : +800m pour les continents, -4000m pour les océans.


Quelle est la roche la plus représentative de la croûte continentale ? 
1) [ basaltes ](Erreur) 
2) [ péridotite ](Erreur) 
3) [ granite ](Struc q3) 
4) [ gabbro ](Erreur) 


## Struc q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La croute océanique est prinicpalement constituée de roches magmatiques. Lesquelles ?
1) [ basalte ](Struc q4)
2) [ péridotite ](Erreur)
3) [ granite ](Erreur)
4) [ gabbro ](Struc q4) 


## Struc q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Il y avait deux bonnes réponses, les as-tu trouvées ?

Si non, tente de trouver la seconde roche.

Si oui, passe à la suite.

4) [Passer à la suite](Struc q4bis) 


## Struc q4bis
La croûte océanique est bien composée de deux roches magmatiques, le **basalte** en surface et le **gabbro** en profondeur. Elle peut aussi être recouverte d'une **couverture sédimentaire**.


Quelle est la roche principale contenue dans le manteau ?

1) [péridotite](Struc q5)
2) [gabbro](Erreur)
3) [roches sédimentaires](Erreur)
4) [basalte](Erreur)
5) [granite](Erreur)


## Struc q5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La **péridotite** est la roche majoritaire du manteau. Plus on va profondément dans le manteau, plus la structure cristalline de ses minéraux tend à se réarranger vers des formes plus compactes en raison de la pression.

Le Moho est la discontinuité entre la croûte et le manteau. Quelle est la profondeur moyenne du Moho en millieu continental?

1) [12 km](Erreur)
2) [20 km](Erreur)
3) [30 km](Struc q5bis)
4) [50 km](Erreur)


## Struc q5bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo}

Le Moho se situe en moyenne à **30 km** de profondeur en milieu continental.
Cela n'est cependant qu'une moyenne, la croûte continentale peut être :
* plus fine, donc le Moho moins profond, dans des zones en extension comme les rifts (ex: le rift est-africain)
* plus épaisse, donc le Moho plus profond, dans des zones de collision comme les chaînes de montagne (ex: sous l'Himalaya le Moho peut atteindre 90 km de profondeur)

Tu as atteint le niveau 1 des révisions sur la structure de la Terre !

1. [Passer au niveau 2](Struc q6)
2. [Retour au message initial]()


## Struc q6
Quel est le nom de la plus profonde discontinuité ?
1) [Lehmann](Struc q7)
2) [Moho](Erreur Moho)
3) [Gutenberg](Erreur Gutenberg)
4) [LVZ](Erreur LVZ)


## Erreur LVZ
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
@{erreur}

Non seulement la LVZ est plutôt vers la surface, mais **ce n'est pas à proprement parler une discontinuité** : elle n'est pas une limite entre deux couches de compositions différentes.

La LVZ est une zone de ralentissement des ondes sismiques dans le manteau supérieur, sur une épaisseur comprise environ entre -100 et -200 km de profondeur. Cela correspond à des pressions et températures où la **péridotite** acquiert un comportement **ductile**, alors qu'elle est cassante dans la partie supérieure du manteau supérieur.

@{encourage}

## Erreur Gutenberg
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
@{erreur}

La discontinuité de Gutenberg est la limite entre le manteau et le noyau. Elle est située à **-2900 km**, mais ce n'est pas la plus profonde.

@{encourage}

## Erreur Moho
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
@{erreur}

La discontinuité de Mohorovicic, plus souvent appelée le Moho, est la limite entre les croûtes terrestres et le manteau. C'est la discontinuité **la moins profonde** ! Sa profondeur est variable : **-12 km sous les océans, en moyenne -30 km sous les continents**.

@{encourage}


## Struc q7
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La discontinuité la plus profonde, située à **-5100 km**, est celle découverte par **Inge Lehmann** en 1936. Elle constitue la limite entre le noyau externe liquide et le noyau interne solide.

De quoi est majoritairement composé le noyau interne et externe de la Terre ? 

1) [granite](Erreur)
2) [péridotite](Erreur)
3) [silicium](Erreur)
4) [fer](Struc q8)

## Struc q8
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Le noyau est en effet composé à **80% de fer**. 
Ses roches, extrêmement denses (jusqu'à 12), sont à l'**état liquide dans le noyau externe**, et à l'**état solide dans le noyau interne**.

Les mouvements des roches liquides du noyau externe sont à l'origine du champ magnétique terrestre.

Comment se nomme le modèle de la structure terrestre ?

1) [le modèle PREM](Struc q9)
2) [le modèle IRM](Erreur) 
3) [le modèle PERM](Erreur) 
4) [le modèle PREUM'S](Erreur) 

## Struc q9
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Il s'agit du modèle PREM : Preliminary Reference Earth Model.
Écris "SUITE" pour passer à la suite.

*Contenu généré par IA, garder l'esprit critique :*
`!useLLM`
Explique quand et comment a été conçu le "modèle PREM" en géologie. Indique toujours la signification de l'acronyme : Preliminary Reference Earth Model.
`END !useLLM`
*Fin du contenu généré par IA.*

!Next: Struc q9bis

## Struc q9bis
- SUITE

Connais-tu parfaitement le modèle PREM de la structure de la Terre ?
Légende ce schéma interactif.

<iframe src="https://learningapps.org/watch?v=ps1juon4j24" style="border:0px;width:100%;height:500px" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>

1. [Question suivante](Struc q9ter) 

## Struc q9ter
Quelles sont les ondes sismiques les plus rapides ?

1) [Les ondes P](Struc q10)
2) [Les ondes S](Erreur)
3) [Les ondes LVZ](Erreur)
4) [Les ondes L](Erreur)


## Struc q10
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

**Les ondes P sont bien les plus rapides.** 
Fun fact : toutes les propositions sont des types d'ondes sauf "les ondes LVZ" qui n'existent pas.

Les **ondes P** sont les **p**remières arrivées, donc les plus rapides. Ce sont des ondes de compression-décompression qui traversent tous les milieux.

Les **ondes S** arrivent en **s**econd, elle sont donc moins rapides. Ce sont des ondes de cisaillement vertical qui traversent uniquement les milieux solides.

Les ondes L sont les plus tardives et les plus destructrices.

**Dernière question !**
Que signifie LVZ ?

1) [Low Velocity Zone](Struc Fin)
2) [Light Vacation Zap](Erreur)
3) [Little Value Zone](Erreur)
4) [Live Vibes Zoom](Erreur)
5) [Last Valuable Zone](Erreur)
6) [Least Velocity Zone](Erreur) 
7) [Low Virtual Zone](Erreur)
8) [Lenteur de la Vitesse Z](Erreur)
9) [Long Variety Zone](Erreur)

## Struc Fin
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

La LVZ est la **Low Velocity Zon**e, ou zone de faible vitesse. 

Les ondes sismiques P et S y sont ralenties, entre 100 et 200km de profondeur, dans le manteau supérieur. Les péridotites y atteignent une pression et températures où elles changent de comportement : de cassantes elles deviennent ductiles.

Le haut de la LVZ délimite la **lithosphère rigide** de l'**asthénosphère ductile**.

Félicitations, tu as atteint le niveau 2 de ces révisions sur la structure de la planète Terre !

1. [Réviser d'autres notions de géologie](Terre)
2. [Retour au message initial]()





## Santé

Cette partie est consacrée à la **santé humaine**, qui recouvre de nombreux champs : reproduction, microbiote, maladies infectieuses, défenses de l'organisme, fonctionnement du cerveau, régulation hormonale...

Choisis ton niveau.

1. [Niveau collège-2de](Santé 2de)
2. [Niveau 1ère spécialité SVT](Santé 1spé)
3. [Retour au message initial]()


## Santé 2de

Je te propose une vidéo et un quiz.
* La vidéo te permet de revoir les notions de collège sur l'immunité.
* Dans ce quiz de 10 questions, tu vas réviser des notions du programme de 2de sur le corps humain et la santé.


1. [Voir la vidéo niveau 3e](SantéVid)
2. [Démarrer le quiz niveau 2de](Santé q1)


## SantéVid

Une vidéo de révision des notions de 3e en immunologie, proposée par *Les Grenouilles*, chaîne de Cathy, enseignante de SVT en collège.

<iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=BBcHrGb9Am0&vignette=https://i.ytimg.com/vi/BBcHrGb9Am0/hqdefault.jpg&debut=0&fin=250&largeur=16&hauteur=9" allow="autoplay; fullscreen" frameborder="0" width="700" height="394"></iframe>

1. [Démarrer le quiz niveau 2de](Santé q1)
3. [Retour au menu "Corps humain et santé"](Santé)

## Santé q1

Quelle proposition correspond à des niveaux biologiques correctement ordonnés, du plus grand au plus petit ?
1) [organisme > organe > tissu > cellule > molécule](Santé q2)
2) [organisme > tissu > organe > cellule > molécule](Erreur)
3) [molécule > cellule > tissu > organe > organisme](Erreur)
4) [organisme > tissu > organe > molécule > cellule](Erreur)

## Santé q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Tu as correctement ordonné les différentes échelles au sein du monde vivant. Le schéma ci-dessous te permet de les visualiser, avec des détails du point de vue moléculaire.
![échelles du vivant](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/echelle-du-monde-vivant.jpg)

1. [Question suivante](Santé q2bis)

## Santé q2bis
Continuons !

La mise en place et la mise en fonctionnement de l'appareil sexuel se réalise :
1) [de la fécondation à la puberté](Santé q3)
2) [à partir de la naissance](Erreur)
3) [pendant l'adolescence](Erreur)
4) [uniquement chez les filles](Erreur)


## Santé q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 

Cette mise en place des organes reproducteurs et leur mise en route se réalise sous l'influence d'**hormones**. Ces molécules sont fabriquées par des glandes et sécrétées dans le sang. Comment agissent-elles sur leurs organes-cibles ?

1) [Elles les stimulent électriquement](Erreur)
2) [Elles se fixent à des récepteurs spécifiques sur la membrane des cellules-cibles](Santé q4)
3) [Elles activent la production d'enzymes spécifiques à la surface des organes-cibles](Erreur)
4) [Elles activent la division cellulaire dans les organes cibles](Erreur)

## Santé q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Ce schéma résume les différentes étapes de la communication hormonale.
![schema communication homonale](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/schema_comm_hormonale_-_dijon_modifi%C3%A9.PNG "Source : SVT Dijon, modifié")

<aside>Source : SVT Dijon, modifié</aside>


1. [Question suivante](Santé q4bis)

## Santé q4bis
Continuons !

Le plaisir chez l'humain repose notamment sur :
1) [l'activation de certains neurones dans le cerveau](Santé q5)
2) [l'activation des muscles squelettiques](Erreur)
3) [la libération d'insuline par le pancréas](Erreur)
4) [l'activation de la moelle épinière](Erreur)

## Santé q5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Le **circuit de la récompense** est constitué de différentes zones du cerveau, elles-mêmes constituées de neurones interconnectés, qui s'activent lors d'une stimulation agréable et libèrent de la dopamine. La libération de cette molécule dans le cerveau provoque une sensation de plaisir pour l'individu.

On peut retenir de cet exemple que **des mécanismes moléculaires et cellulaires ont un impact à l'échelle des organes et de l'organisme**.

Changeons de sujet...

Chlamydia, Papillomavirus HPV, VIH... Ces micro-organismes ont un point commun : lequel ?
1) [Ils se transmettent tous par voie sexuelle](Santé q6)
2) [Ce sont tous des virus](Erreur1)
3) [Ils se transmettent tous par voie aérienne](Erreur2)
4) [Ce sont des micro-organismes "géants" d'environ 0,1 mm](Erreur3)

## Santé q6
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo} 

Ces micro-organismes se transmettent tous par voie sexuelle, certains comme le VIH se transmettent aussi par voie sanguine. Ils sont à l'origine des **IST** : infections sexuellement transmissibles.

Bravo, tu as atteint le niveau 1.

1. [En savoir plus sur la prévention des IST](!useLLM Explique quelles sont les mesures de prévention des infections sexuellement transmissibles.)
2. [Passer au niveau 2](Santé q6bis)
3. [Retour au message initial]()

## Santé q6bis
C'est parti pour 5 questions supplémentaires !

En parlant de micro-organismes, tu as dû entendre parler du microbiote. De quoi s'agit-il ?

1) [l'ensemble des microorganismes vivant dans et sur le corps humain](Santé q7)
2) [les cellules immunitaires du corps humain](Erreur1)
3) [les hormones circulant dans le sang](Erreur2)
4) [les toxines et les molécules de défense produites par les cellules humaines](Erreur3)

## Santé q7
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Le **microbiote** est bien l'ensemble des microorganismes vivant dans et sur le corps humain. Il colonise la peau et les muqueuses comme notre tube digestif ou les muqueuses génitales.

Comment nomme-t-on notre relation avec notre microbiote ?
1) [Symbiose](Santé q8)
2) [Parasitisme](Erreur1)
3) [Prédation](Erreur2)
4) [Commensalisme](Erreur3)

## Santé q8
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

C'est bien une **symbiose** : une association à bénéfice réciproque.

Tous les types de relations proposés à la question précédente existent dans le monde vivant. 

1. [En savoir plus sur la symbiose](!useLLM Explique au niveau scientifique expert ce qu'est une relation symbiotique et donne un exemple.)
2. [En savoir plus sur la prédation](!useLLM Explique au niveau scientifique expert ce qu'est une relation prédateur-proie et donne un exemple.)
3. [En savoir plus sur le commensalisme](!useLLM Explique au niveau scientifique expert ce qu'est le commensalisme et donne un exemple.)
4. [En savoir plus sur le parasitisme](!useLLM Explique au niveau scientifique expert ce qu'est le parasitisme et donne un exemple.)
5. [Question suivante](Santé q8bis)



## Santé q8bis

Les agents pathogènes peuvent être :
1) [des bactéries, des virus, des champignons ou des eucaryotes parasites](Santé q9)
2) [des virus ou des bactéries uniquement](Erreur)
3) [des bactéries uniquement](Erreur)
4) [des bactéries et des eucaryotes parasites uniquement](Erreur)


## Santé q9
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les agents pathogènes peuvent appartenir à **tous les types de micro-organismes**. Mais tous les micro-organismes ne sont pas pathogènes bien sûr ! Certains sont bénéfiques, comme ceux du microbiote.

Les agents pathogènes vivent aux dépens d’un autre organisme, tout en lui portant préjudice (les symptômes). Cet organisme devient leur milieu de vie.

Comment nomme-t-on cet organisme ?

1) [hôte](Santé q10)
2) [donneur](Erreur)
3) [receveur](Erreur)
4) [vecteur](Erreur)

## Santé q10
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les agents pathogènes vivent aux dépens d’un autre organisme, appelé **hôte**.

La propagation du pathogène se fait par changement d’hôte. Il se fait soit par contact entre hôtes, soit par le milieu ambiant (air, eau), soit par un **vecteur biologique** qui est alors l’agent transmetteur indispensable du pathogène.

1. [En savoir plus](!useLLM Explique le mode de propagation de la maladie de Lyme à un niveau scientifique expert en utilisant les mots : pathogène, hôte, vecteur, cycle. Sois précis mais bref.)
2. [Passer à la dernière question du quiz](Santé q10bis)


## Santé q10bis

Le VIH, la grippe, sont des exemples de maladies à transmission directe.
Le paludisme, la maladie de Lyme, sont des exemples de maladies vectorielles.

Dans tous les cas, la propagation des pathogènes peut être plus ou moins rapide et provoquer une **épidémie**. 

Quel(s) comportement(s) permettent de limiter la propagation d'un pathogène ?


1) [la vaccination](vaccination)
2) [les mesures d'hygiène](hygiène)
3) [les mesures de protection](protection)
4) [les incantations](Erreur)


## vaccination
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La **vaccination** est une mesure préventive permettant d'empêcher le développement et les conséquences parfois mortelles de certaines infections comme la rougeole, la grippe, l'infection au HPV (papillomavirus) ou encore les hépatites A et B.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Santé Fin)

## hygiène
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les **mesures d'hygiène** sont essentielles pour limiter les risques d'infection et de propagation des pathogènes : se laver les mains régulièrement, se moucher dans un mouchoir jetable (et le jeter à la poubelle !), éternuer dans son coude...  

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Santé Fin)

## protection
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les **mesures de protection** permettent de repousser ou éliminer les pathogènes ou leurs agents de transport : porter des vêtements longs dans les zones à risque (maladies vectorielles), utiliser un produit répulsif, éliminer les eaux stagnantes où se reproduisent les moustiques, porter un masque, porter un préservatif lors d'un rapport sexuel...

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Santé Fin)

## Santé Fin
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

Il y avait trois bonnes réponses possibles ! Les comportements permettant de limiter la propagation d'un pathogène sont :
- la **vaccination**, si elle existe pour ce pathogène
- les **mesures d'hygiène** : se laver les mains, éternuer dans son coude...
- les **mesures de protection** : porter des vêtements longs dans les zones à risque (maladies vectorielles), utiliser un produit répulsif, éliminer les eaux stagnantes, porter un masque...

Félicitations, tu as atteint le niveau 2 de ces révisions de 2de sur le corps humain et la santé !

1. [Passer au niveau 1ère spécialité SVT](Santé 1spé)
2. [Retour au message initial]()


## Santé 1spé

Que souhaites-tu réviser ?

1. [La réaction immunitaire innée](Santé 1spé q1)

## Santé 1spé q1
Quels sont les symptôme de la réaction inflammatoire ?

1) [Gonflement, chaleur, douleur, rougeur](Santé 1spé q2)
2) [Rougeur, chaleur, douleur, transpiration](Erreur)
3) [Douleur, chaleur, couleur, gonflement](Erreur)
4) [Chaleur, diapédèse, fièvre et pus](Erreur)


## Santé 1spé q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Rougeur, chaleur, douleur et gonflement sont les 4 symptômes de la réaction inflammatoire décrits par Celsus au 1er siècle après JC.

Les cellules de l’immunité innée sont :

1) [Les leucocytes](Santé 1spé q3)
2) [Les globules rouges](Erreur)
3) [Les médiateurs chimiques de l’inflammation](Erreur échelle)
4) [Les organes lymphoïdes](Erreur échelle)

## Santé 1spé q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Ce sont bien les **leucocytes**, ou globules blancs.

Les cellules de l’immunité circulent dans l’organisme par :

1) [Les vaisseaux lymphatiques et les vaisseaux sanguins](Santé 1spé q4)
2) [Les vaisseaux sanguins uniquement](Erreur)
3) [Les vaisseaux lymphatiques uniquement](Erreur)
4. [Le pus](Erreur)

## Santé 1spé q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Qu’est-ce qui peut déclencher une réaction inflammatoire aiguë ?

1) [Les micro-organismes pathogènes, les toxines, des agents externes comme les UV](Santé 1spé q5)
2) [Tous les micro-organismes, les toxines, des agents externes comme les UV](Erreur)
3) [Les micro-organismes pathogènes uniquement](Erreur)
4) [Tous les micro-organismes](Erreur)
5. [Le pus](Erreur)

## Santé 1spé q5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

En effet, les agents agresseurs sont nombreux ! Toxines, venins, agents externes pouvant causer des mutations dans nos cellules, et les micro-organismes comme des bactéries, virus, champignons, eucrayotes unicellulaires...

Mais tous les micro-organismes ne sont pas dangereux : certains sont en symbiose, comme le microbiote intestinal qui nous aide à digérer, ces micro-oragnismes ne doivent pas être éliminés.

La reconnaissance des éléments pathogènes par les cellules sentinelles se réalise :

1) [grâce à leurs récepteurs PRR qui reconnaissent les motifs PAMP des familles de pathogènes](Santé 1spé q6)
2) [grâce à leurs récepteurs PRR qui reconnaissent les motifs PAMP précis des pathogènes](Erreur)
3) [grâce à leurs récepteurs PAMP qui reconnaissent les motifs PRR des familles de pathogènes](Erreur)
4) [grâce à leurs récepteurs PAMP qui reconnaissent les motifs PRR précis des pathogènes](Erreur)
5. [grâce au pus](Erreur)

## Santé 1spé q6
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo} 

Les cellules sentinelles possèdent des récepteurs dits PRR (*Pattern Recognition receptor*) qui reconnaissent les motifs moléculaires communs (PAMP : *Pathogen Associated Molecular Patterns*) aux grandes familles de pathogènes.

Cette fixation active les cellules sentinelles, qui sécrètent alors des molécules nommées **médiateurs chimiques de l'inflammation** et se lancent dans la **phagocytose**.

Tu as atteint le niveau 1.

2. [Passer au niveau 2](Santé 1spé q6bis)
3. [Retour au message initial]()

## Santé 1spé q6bis

Qu’est-ce qui provoque les sensations douloureuses lors de la réaction inflammatoire ?

1) [Certains médiateurs chimiques activent des fibres nerveuses spécialisées dans la douleur](Santé 1spé q7)
2) [Certains médiateurs chimiques provoquent une vasodilatation](Erreur)
3) [Certains médiateurs chimiques activent des fibres nerveuses spécialisées dans le mouvement](Erreur)
4) [L’accumulation des globules blancs et de débris constitue du pus dont la présence est douloureuse](Erreur)
5. [Le pus](Erreur)

## Santé 1spé q7
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les fibres nerveuses de type C sont stimulées par des molécules inflammatoires (ex : la bradykinine), ce qui génère une sensation de **douleur**. Cette sensation est importante pour avertir l'individu de l'infection débutante.

Quelle molécule est responsable de la vasodilatation lors de la réaction inflammatoire ?

1) [L’histamine](Santé 1spé q8)
2) [La COX](Erreur)
3) [L'ADN polymérase](Erreur)
4) [Les monocytes](Erreur échelle)
5. [Le pus](Erreur échelle)

## Santé 1spé q8
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

L'histamine est une molécule sécrétée par les mastocytes. Elle permet la vasodilatation, c'est à dire la dilatation des vaisseaux sangouins, dnas la zone infectée. Ceci augmente l'afflux sanguin, avec pour conséquence, la **rougeur** et la **chaleur** de la zone.

L'afflux sanguin important participe au recrutement des globules blancs (leucocytes) circulants, qui viennent alors en "renfort".

Le phénomène de reconnaissance de la zone infectée par les leucocytes circulants, d’adhésion à la paroi des vaisseaux et de franchissement de cette paroi, se nomme :

1) [La diapédèse](Santé 1spé q9)
2) [La phagocytose](Erreur)
3) [La réaction enzymatique](Erreur)
4) [L’anti-inflammatoire](Erreur)
5. [Le pus](Erreur)

## Santé 1spé q9
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Il s'agit bien de la **diapédèse**.

Cette vidéo sans son vous permet de visualiser le phénomène sur une animation et une vidéo (accélérée).
<iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=F5x53PhegI0&vignette=https://i.ytimg.com/vi/F5x53PhegI0/hqdefault.jpg&debut=0&fin=61&largeur=16&hauteur=9" allow="autoplay; fullscreen" frameborder="0" width="700" height="394"></iframe>

La cellule présentatrice d’antigène « professionnelle » est :
1) [La cellule dendritique](Santé 1spé q10)
2) [Le monocyte](Erreur)
3) [Le phagocyte](Erreur)
4) [Le granulocyte](Erreur)
5. [Le pus](Erreur échelle)

## Santé 1spé q10
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

L’ibuprofène est un médicament dont l’action :

1) [Réduit les symptômes désagréables de la réaction inflammatoire sans entraver l’action des cellules phagocytaires](Santé 1spé Fin)
2) [Réduit les symptômes désagréables de la réaction inflammatoire et entrave l’action des cellules phagocytaires](Erreur)
3) [Accroît les symptômes désagréables de la réaction inflammatoire sans entraver l’action des cellules phagocytaires](Erreur)
4) [Accroît les symptômes désagréables de la réaction inflammatoire et entrave l’action des cellules phagocytaires](Erreur)

## Santé 1spé Fin
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

Félicitations, tu as atteint le niveau 2 de ces révisions de 1ère spécialité SVT sur la réaction immunitaire innée !

2. [Retour au message initial]()


## Génétique

La **génétique** est l'étude de la transmission des caractères héréditaires chez les êtres vivants. 

Elle vise à déterminer les modes de transmission et à documenter les variations dans les gènes entre les individus d'une même espèce.

Choisis ton niveau.

1. [Niveau collège-2de](Gen 2de)
2. [Niveau 1ère spécialité SVT](Gen 1spé)
3. [Retour au message initial]()


## Gen 2de

C'est parti pour un petit quiz de génétique !

Les nucléotides sont des molécules constitutives de l'ADN.

Comment qualifie-t-on la structure de l'ADN ?

1) [simple brin](Erreur)
2) [double-hélice](suite gen1)
3) [entortillée](Erreur)
4) [tricaténaire](Erreur)


## suite gen1
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

L'ADN est une molécule constituée de nucléotides associés en deux chaînes, ou brins, enroulées en **double-hélice**.

**Schéma animé de la structure en double hélice de l'ADN** 

![GIF ADN](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/DNA_animation.gif "Source: Wikimédia, domaine public")
<aside>Source: Wikimédia, domaine public</aside>

**Schéma de la structure moléculaire de l'ADN**

![schéma ADN](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/DNA_structure_and_bases_FR.svg.png "Wikimédia, CC BY SA Dosto")
<aside>Source: Wikimédia, CC BY SA Dosto</aside>

1. [En savoir plus](!useLLM Explique la structure de l'ADN.)
2. [Question suivante](suite gen2)

## suite gen2

Les nucléotides qui se font face sont complémentaires 2 à 2.

Quel est le nucléotide complémentaire de A ?
`@nc = A`

---
Les nucléotides qui se font face sont complémentaires 2 à 2.

Quel est le nucléotide complémentaire de T ?
`@nc = T`

---
Les nucléotides qui se font face sont complémentaires 2 à 2.

Quel est le nucléotide complémentaire de G ?
`@nc = G`

---
Les nucléotides qui se font face sont complémentaires 2 à 2.

Quel est le nucléotide complémentaire de C ?
`@nc = C`

1. [A @ncAnswer=A](question nucléotide)
2. [T @ncAnswer=T](question nucléotide)
3. [G @ncAnswer=G](question nucléotide)
4. [C @ncAnswer=C](question nucléotide)

## question nucléotide

`if @nc==A && @ncAnswer==T`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}
A et T sont bien 2 nucléotides complémentaires.
`endif`

`if @nc==T && @ncAnswer==A`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
A et T sont bien 2 nucléotides complémentaires.
`endif`

`if @nc==C && @ncAnswer==G`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
C et G sont bien 2 nucléotides complémentaires.
`endif`

`if @nc==G && @ncAnswer==C`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
C et G sont bien 2 nucléotides complémentaires.
`endif`

`if @nc==G && !(@ncAnswer==C)`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
C'est une erreur ! Réessaye.
`endif`

`if @nc==C && !(@ncAnswer==G)`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
C'est une erreur ! Réessaye.
`endif`

`if @nc==A && !(@ncAnswer==T)`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
C'est une erreur ! Réessaye.
`endif`

`if @nc==T && !(@ncAnswer==A)`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
C'est une erreur ! Réessaye.
`endif`


1. [Encore des nucléotides](suite gen2)
2. [Question suivante](suite gen2bis)


## suite gen2bis
`@KEYBOARD = true`
Tape en majuscules la séquence complémentaire de celle-ci : 
GATC

!Next: Bonne réponse q1 / Tu n'as pas donné la bonne séquence, essaie encore !


## Bonne réponse q1
- CTAG

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}
La séquence complémentaire de : 
GATC  est bien : 
CTAG

Tu as bien placé un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.

1. [Me poser une question similaire](suite gen2ter)
2. [Passer à la question suivante](suite gen3)


## suite gen2ter
`@KEYBOARD = true`
Tape en majuscules la séquence complémentaire de celle-ci : 
CTAATGT

1. [Je ne sais pas / Voir la réponse](GATTACA sais pas)

!Next: GATTACAok / Tu n'as pas donné la bonne séquence, essaie encore !


## GATTACAok
- GATTACA

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
La séquence complémentaire de : 
CTAATGT  est bien : 
GATTACA

Tu as bien placé un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.

1. [Me poser une question similaire](GATTACA suite)
2. [Passer à la question suivante](suite gen3)


## GATTACA sais pas

La séquence complémentaire de : 
CTAATGT  est : 
GATTACA

Il faut placer un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.

1. [Réessayer](GATTACA suite)
2. [Question suivante](suite gen3)


## GATTACA suite
`@KEYBOARD = true`
Tape en majuscules la séquence complémentaire de celle-ci : 
ATCATGTTG

1. [Je ne sais pas / Voir la réponse](TAGTACAAC sais pas)

!Next: TAGTACAACok / Tu n'as pas donné la bonne séquence, essaie encore !

## TAGTACAACok
- TAGTACAAC

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
La séquence complémentaire de : 
ATCATGTTG  est bien : 
TAGTACAAC

Tu as bien placé un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.

1. [Question suivante](suite gen3)



## TAGTACAAC sais pas

La séquence complémentaire de : 
ATCATGTTG  est : 
TAGTACAAC

Il faut placer un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.

1. [Question suivante](suite gen3)



## suite gen3

L'ADN est constitutif de quel(s) élément(s) de la cellule ? 
Une seule bonne réponse.

1) [Les chromosomes](suite gen4)
2) [La membrane plasmique](Erreur1)
3) [L'enveloppe nucléaire](Erreur2)
4) [Le cytoplasme](Erreur3)

## suite gen4

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les **chromosomes** sont constitués d'ADN.

La succession des nucléotides le long des chromosomes constitue un ensemble de messages que la cellule peut "lire" : c'est **l'information génétique**.

Combien une cellule humaine (gamètes non compris) contient-elle de chromosomes ?

1) [23 paires](suite gen4bis)
2) [23](Erreur1)
3) [46 paires](Erreur2)
4) [46](suite gen4bis)

## suite gen4bis
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 

Il y avait deux bonnes réponses possibles :wink: 
Une cellule humaine contient **46 chromosomes**, qui correspondent à **23 paires de chromosomes**. On les appelle des paires de chromosomes homologues.

1. [En savoir plus](!useLLM Explique ce que sont des chromosomes homologues.)
2. [Question suivante](suite gen4ter)

## suite gen4ter
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

Bravo pour ta persévérance : tu as atteint le niveau 1.

1. [En savoir plus](!useLLM Explique ce qu'est le caryotype d'un individu.)
2. [Passer au niveau 2](suite gen4-4)
3. [Retour au message initial]()

## suite gen4-4

C'est parti pour 5 questions supplémentaires !

Revenons à la séquence d'ADN... Comment nomme-t-on une séquence de nucléotides permettant de coder un caractère héréditaire ?

1) [Un gène](suite gen5)
2) [Un allèle](Erreur1)
3) [Un texto](Erreur2)
4) [Une mutation](Erreur3)

## suite gen5

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Un **gène** est une séquence d'ADN qui code pour un caractère héréditaire, ou plus précisément pour une protéine responsable d'un caractère héréditaire.

Cette information génétique peut subir des modifications de manière aléatoire :  quel nom donne-t-on à cette modification de la séquence d'ADN ?

1) [Une transmutation](Erreur1)
2) [Une mutation](suite gen6)
3) [Une coquille](Erreur2)
4) [Un allèle](Erreur3)

## suite gen6

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

C'est une **mutation** : un nucléotide (ou plusieurs) peut être remplacé par un autre, ou supprimé, ou ajouté.

1. [En savoir plus](!useLLM Explique ce qu'est une mutation et les conséquences possibles.)
2. [Question suivante](suite gen6bis)

## suite gen6bis

Si cette nouvelle version du gène est transmise aux générations suivantes, comment l'appellera-t-on ?

1) [un allèle du gène](suite gen7)
2) [un gène de l'allèle](Erreur1)
3) [un clone du gène](Erreur2)
4) [un allèle du clone](Erreur3)
5) [l'attaque des clones](clone)

## clone
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
Tu l'as fait exprès n'est-ce pas ? :laughing: 

Allez réessaye.

## suite gen7

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

C'est en effet un nouvel **allèle** du gène qui est apparu dans la population !

Comment nomme-t-on un individu qui possède deux allèles identiques d'un même gène ?

1) [hétérozygote](Erreur2)
2) [clone](Erreur1)
3) [homozygote](suite gen8)
4) [dizygote](Erreur3)

## suite gen8

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Avec deux allèles identiques d'un même gène, un individu est qualifié d'**homozygote** pour ce gène.


Comment nomme-t-on un individu qui possède deux allèles différents d'un même gène ?

1) [hétérozygote](suite gen9)
2) [clone](Erreur1)
3) [homozygote](Erreur2)
4) [monozygote](Erreur3)


## suite gen9

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo} 

Si les deux allèles sont différents, il est qualifié d'**hétérozygote** pour ce gène.

Tu as atteint le niveau 2.

On s'arrête ici pour les révisions niveau 2de !

1. [Passer au niveau 1ère spécialité SVT](Gen 1spé)
2. [Retour au message initial]()

---

## Gen 1spé

Les travaux ont débuté... :construction_worker_woman: 

**Vos questions sélectionnées par votre professeure apparaîtront progressivement.**

Que souhaites-tu réviser ?

1. [Les divisions cellulaires](Div 1spé)
2. [Le cycle cellulaire et la réplication](Cycle 1spé)
3. [Retour au message initial]()



## Div 1spé

C'est parti pour quelques questions de révision !

Le processus de division cellulaire qui produit deux cellules filles identiques est appelé : 
1) [La méiose](Erreur)
2) [La caryogamie](Erreur)
3) [La mitose](Div q2)
4) [La division réductionnelle](Erreur)

## Div q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Dans quelle phase de la méiose les chromosomes homologues se séparent-ils ?

1) [Prophase I](Erreur)
2) [Métaphase I](Erreur)
3) [Anaphase I](Div q3)
4) [Télophase II](Erreur)
5) [Anaphase II](Erreur)

## Div q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La méiose conduit à la formation de : 

1) [Deux cellules diploïdes](Erreur)
2) [Deux cellules haploïdes](Erreur)
3) [Quatre cellules haploïdes](Div q4)
4) [Quatre cellules diploïdes](Erreur)
5) [Une cellule haploïde](Erreur)


## Div q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Comment expliquer que les gamètes contiennent la moitié de l'information génétique des cellules germinales dont elles sont issues ?
1) [Durant l'anaphase 2, il y a eu séparation des chromatides-sœurs. La moitié de l'information génétique a donc été perdue.](Erreur)
2) [Il y a eu une mitose, la quantité d'ADN a donc été divisée par deux.](Erreur)
3) [Durant l'anaphase 1, il y a eu séparation des chromosomes homologues et les cellules sont devenues haploïdes.](Div q5)

## Div q5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Comment appelle-t-on la lignée de cellules dont font partie les gamètes ?

1) [lignée gamétique](Erreur)
2) [lignée reproductrice](Erreur)
3) [lignée germinale](Div niv1)
4) [lignée spermatique](Erreur)
 

## Div niv1

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo} 

Tu as réussi une série de 5 questions sur les divisions cellulaires.

1. [Continuer pour atteindre le niveau 2](Div q6)
2. [Retourner au menu de génétique de 1ère](Gen 1spé)

## Div q6

C'est parti pour une nouvelle série de 5 questions sur les divisions cellulaires !

Comment qualifie-t-on la première division de la méiose?

1) [équationnelle](Erreur)
2) [proméiotique](Erreur)
3) [réductionnelle](Div q7)
4) [mathématique](Erreur)

## Div q7

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La formule chromosomique d’un gamète chez l’être humain est : 
1) [2n = 46](Erreur)
2) [2n = 23](Erreur)
3) [n = 23](Div q8)
4) [n = 46](Erreur)

## Div q8

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Comment s’appelle la phase de la mitose durant
laquelle les chromosomes s’alignent sur le plan équatorial de la cellule ?

1) [La métaphase](Div q9)
2) [La prophase](Erreur)
3) [La télophase](Erreur)
4) [L’anaphase](Erreur)

## Div q9

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Lors de la méiose, à quelle étape les chromatides-sœurs se séparent ?  

1) [Télophase II](Erreur)
2) [Anaphase I](Erreur)
3) [Anaphase II](Div q10)
4) [Métaphase I](Erreur)
    
## Div q10

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Grâce à quoi la double hélice d’ADN peut se condenser/ décondenser lors des divisions cellulaires ? 
1) [grâce à l'adénine](Erreur)
2) [grâce au désoxyribose](Erreur)
3) [grâce à des protéines histones](Div Fin)
4) [grâce à leurs groupements phosphate](Erreur)
    
## Div Fin

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

Félicitations ! 
Tu as atteint le 2e niveau, tu en connais un rayon sur les divisions cellulaires !

1. [Retourner au menu de génétique de 1ère](Gen 1spé)


## Cycle 1spé

C'est parti pour quelques questions de révision sur le cycle cellulaire et la réplication !

Quelle phase du cycle cellulaire est responsable de la réplication de l'ADN ?
1) [G1](Erreur)
2) [S](Cycle q2)
3) [G2](Erreur)
4) [Mitose (M)](Erreur)

## Cycle q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La réplication de l'ADN a bien lieu en phase S.

La réplication de l'ADN est dite :
1) [Conservative](Erreur)
2) [Semi-conservative](Cycle q3)
3) [Dispersive](Erreur)
4) [Equationnelle](Erreur)

## Cycle q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

L'ADN se réplique de façon semi-conservative. 

1. [En savoir plus](!useLLM Explique comment l'ADN est répliqué de manière semi-conservative.)
2. [Question suivante](Cycle q3bis)

## Cycle q3bis

Lors de quelle phase du cycle cellulaire la quantité d'ADN passe de 2Q à Q ?

1) [G1](Erreur)
2) [S](Erreur)
3) [G2](Erreur)
4) [Mitose (M)](Cycle q4)

## Cycle q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Que permet le dédoublement de l'information génétique avant la mitose ?

1) [Augmenter la taille et la maturité des cellules](Erreur)
2) [Chaque cellule fille reçoit ainsi une copie complète de l'information génétique](Cycle q5)
3) [De mieux répondre aux conditions de stress](Erreur)
4) [De réaliser la caryogamie lors de la fécondation](Erreur)


## Cycle q5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Est ce que la mitose conserve la ploïdie de la cellule ?

1) [Non](Erreur)
2) [Oui](Cycle niv1)

## Cycle niv1

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo} 

Tu as réussi une série de 5 questions sur le cycle cellulaire et la réplication. Tu as atteint le niveau 1.

1. [Continuer pour atteindre le niveau 2](Cycle q6)
2. [Retourner au menu de génétique de 1ère](Gen 1spé)

## Cycle q6

Comment se nomme l'enzyme responsable de la réplication de l'ADN ?

1) [ARN polymérase](Erreur)
2) [ADN polymérase](Cycle q7)
3) [ADN polymérose](Erreur)
4) [Histone](Erreur)

## Cycle q7
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

L'ADN polymérase est...

1) [une cellule](Erreur échelle)
2) [une molécule](Cycle q8)
3) [un organite](Erreur échelle)
4) [un tissu](Erreur échelle)

## Cycle q7
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}
En effet l'ADN polymérase est une molécule, plus précisément une protéine ayant une fonction d'enzyme (catalyseur biologique).

1) [La mitose](Erreur)
2) [L’œil de réplication](Cycle q8)
3) [La fourche de réplication](Erreur)
4) [La méiose](Erreur)

## Cycle q8
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Qu'est-ce qui permet la conservation du caryotype **d'une génération à l'autre** lors du cycle de développement d'une espèce diploïde comme l'être humain ?

1) [L’alternance mitose/fécondation](Erreur)
2) [L’alternance méiose/fécondation](Cycle q9)
3) [L’alternance mitose/méiose](Erreur)

## Cycle q9
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Comment nomme-t-on un ensemble de cellules toutes identiques ?

1) [Un clown](Erreur)
2) [Un clone](Cycle q10)
3) [Un cône](Erreur)
4. [George Clowney](Erreur)

## Cycle q10
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Ordonne les étapes et résultats de l'expérience de Meselson et Stahl sur des clones bactériens aux cycles synchronisés.

A-Au bout d'un cycle cellulaire, l'ADN est prélevé, centrifugé : 50% de l'ADN a un poids moléculaire intermédiaire, 50% de l'ADN a un poids moléculaire faible.
B-L'ADN est prélevé, centrifugé : tout l'ADN a un poids moléculaire élevé.
C-Les bactéries sont cultivées dans un milieu contenant de l'azote lourd.
D-Au bout d'un cycle cellulaire, l'ADN est prélevé, centrifugé : tout l'ADN a un poids moléculaire intermédiaire.
E-Les bactéries sont cultivées dans un milieu contenant de l'azote léger.

1) [C B D E A](Erreur)
2) [C B E D A](Cycle Fin)
3) [E A C B D](Erreur)
4. [B C A E D](Erreur)

## Cycle Fin

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo} 

Tu as un **niveau expert** sur le cycle cellulaire et la réplication !

1. [Retourner au menu de génétique de 1ère](Gen 1spé)

---

## Biodiversité
- back
- BACK
La **biodiversité** désigne l’ensemble des êtres vivants ainsi que les écosystèmes dans lesquels ils vivent. Ce terme comprend également les interactions des espèces entre elles et avec leurs milieux. 

La biodiversité a changé depuis l'apparition des premières formes de vie sur Terre. Elle a **évolué** et continue de changer actuellement.

Choisis ton niveau.

1. [Niveau collège-2de](Biodiv 2de)
2. [Niveau 1ère spécialité SVT](Biodiv 1spé)
3. [Retour au message initial]()


## Biodiv 1spé


Chouette, nous allons travailler sur la notion d'écosystème au niveau 1ère spécialité SVT !

Ta classe a spécifiquement travaillé sur la construction de **ma base de connaissances** (RAG) sur ce sujet. Ta professeure l'a complétée avec une **liste de vocabulaire** qu'elle m'a fournie.

Tu as le choix entre deux manières de réviser : 

1. [J'interroge ViTa](Eco1)
2. [ViTa m'interroge](Eco2)

## Eco1

D'accord, je vais répondre à tes questions ! Pour cela, ta classe a préparé une liste de prompts. 

*Garde à l'esprit que je ne suis qu'une IA générative qui donne une réponse statistiquement probable, même si ma base de connaissances m'aide à moins me tromper... Conserve un esprit critique !*

Sur quel sujet souhaites-tu m'interroger ?

1. [Écosystème](!useLLM Explique au niveau expert ce qu'est un écosystème.)
2. [Agrosystème](!useLLM Explique au niveau Expert ce qu'est un agrosystème.)
3. [Biome](!useLLM Explique au niveau expert ce qu'est un biome.)
4. [Biotope](!useLLM Explique au niveau expert qu'est ce qu'un biotope.)
5. [Relations entre espèces](!useLLM Explique au niveau expert les différents types de relations entre les espèces au sein d'un écosystème. )
1. [Répartition des êtres vivants](!useLLM Explique au niveau expert par quoi est influencée la répartition des êtres vivants.)
6. [Hétérotrophe](!useLLM Explique au niveau expert ce qu'est un être vivant héterotrophe)
7. [Autotrophe](!useLLM Explique au niveau expert ce qu'est un être vivant autotrophe)
9. [Héliophile](!useLLM Explique au niveau expert qu'est ce que une éspèce heliophile.) 
10. [Facteur abiotique](!useLLM Explique au niveau expert ce qu'est un facteur abiotique.)
8. [Symbiose](!useLLM Explique au niveau expert ce qu'est la symbiose) 
1. [Compétition](!useLLM Explique au niveau Expert la compétition entre deux espèces.)
5. [Parasitisme](!useLLM Explique au niveau expert ce qu'est le parasitisme.)
1. [Climax](!useLLM Explique au niveau expert ce qu'est le climax.)
1. [Succession écologique](!useLLM Explique au niveau expert ce qu'est une succession écologique.)

## Eco2
- PLUS
- plus
`@KEYBOARD = true`

@{repondre}

Donne la définition de : @{def-eco}.

Cite ce mot dans ta réponse pour que je la comprenne bien : 
**"La définition de ..... est ...."** 
ou "Je ne connais pas la définition de ....".

`@reponseeco = @INPUT : Eco2bis`


## Eco2bis
`@KEYBOARD = true`
Écris "PLUS" pour être à nouveau interrogé, ou "BACK" pour revenir au début de la partie sur les écosystèmes.

*Réponse générée par IA, garder l'esprit critique :*

`!useLLM`

Voici une définition d'un élève : `@reponseeco`.

Évalue sa réponse : l'élève doit donner une définition complète et bien rédigée. Evalue sa réponse en lui indiquant si sa réponse est fausse, incomplète, ou correcte. Donne lui des conseils pour améliorer sa réponse. Adresse-toi à lui directement en le tutoyant.

`END !useLLM`

*Fin de la réponse générée par l'IA.*




## Biodiv 2de

Le niveau 1 du quiz explore les différentes **échelles de la biodiversité**, le niveau 2 te questionne sur les **mécanismes de l'évolution**.

1. [C'est parti !](Biodiv q1)
2. [Retour au message initial]()

## Biodiv q1

La plus grande échelle à laquelle on définit la biodiversité est la diversité...

1) [des écosystèmes](Biodiv q2)
2) [génétique](Erreur1)
3) [des espèces](Erreur2)
4) [intra-spécifique](Erreur3)

## Biodiv q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Il existe une grande **diversité d'écosystèmes** à la surface de notre planète : la forêt tempérée, la forêt tropicale, le désert, le littoral, une grotte, l'écosystème urbain, les fonds marins... 

Un écosystème n'est pas forcément très grand : même une flaque d'eau, une poignée de terre ou notre intestin peuvent être considérés comme des écosystèmes, avec une biodiversité très riche !

1. [En savoir plus](!useLLM Explique ce qu'est un écosystème et donne quelques exemples d'écosystèmes.)
2. [Question suivante](Biodiv q2bis)

## Biodiv q2bis

Au sein d'un écosystème, il existe de nombreuses espèces d'êtres vivants en interaction. C'est la biodiversité...

1) [extra-spécifique](Erreur2)
2) [génétique](Erreur1)
3) [spécifique](Biodiv q3)
4) [intra-spécifique](Erreur3)

## Biodiv q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La biodiversité des espèces est aussi appelée la **biodiversité spécifique**.

Mais comment sait-on que deux individus sont de la même espèce ?

1) [ils se ressemblent toujours et peuvent avoir des descendants](Erreur3)
2) [ils vivent dans le même milieu](Erreur1)
3) [ils partagent de nombreux points communs : mode de vie, nourriture, territoire...](Erreur2)
4) [ils peuvent se ressembler, se reproduire ensemble et avoir une descendance fertile](Biodiv q4)


## Biodiv q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

C'est en effet la meilleure réponse !

Deux individus de la même espèce ne partagent pas nécessairement le même milieu ni le même mode de vie. Les critères les plus couramment retenus par les scientifiques sont :
- la ressemblance
- l'interfécondité
- la descendance viable et fertile

Parfois il n'est pas possible de vérifier tous ces critères (espèces fossiles, fort dimorphisme sexuel...), on ne retient que ceux qui sont vérifiables.

1. [En savoir plus](!useLLM Explique pourquoi il est difficile de donner une définition de l'espèce valable dans toutes les situations.)
2. [Question suivante](Biodiv q4bis)

## Biodiv q4bis

Au sein d'une espèce, on observe aussi une diversité entre les individus. On l'appelle biodiversité...

1) [allélique](Biodiv q5)
2) [génétique](Biodiv q5)
3) [écosystémique](Erreur)
4) [intra-spécifique](Biodiv q5)



## Biodiv q5

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

Il y avait trois bonnes réponses à cette question ! 

La biodiversité au sein d'une espèce peut être aussi nommée : 
- biodiversité intra-spécifique
- biodiversité génétique
- biodiversité allélique

1. [En savoir plus](!useLLM Explique comment deux individus d'une même espèce peuvent avoir les mêmes gènes mais être différents.)
2. [Question suivante](Biodiv q5bis)

## Biodiv q5bis

Qu'est-ce qui permet l'existence de plusieurs allèles d'un même gène, engendrant ainsi la biodiversité génétique ?

1) [l'existence des mutations](Évolution q1)
2) [la stabilité de la molécule d'ADN](Erreur)
3) [l'alternance des saisons](Erreur)
4) [la perméabilité de la membrane plasmique](Erreur)

 

## Évolution q1

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav


Bravo ! Tu as atteint le niveau 1.

En effet, au sein de chaque espèce, il existe une diversité génétique due à la présence d'allèles différents d'un même gène, issus de **mutations** survenues au fil des générations. 

1. [En savoir plus](!useLLM Explique les différents types de mutation : par délétion, substitution et insertion.)
2. [Passer au niveau 2](Évolution q1bis)
3. [Retour au message initial]()

## Évolution q1bis

C'est parti pour 5 questions supplémentaires !

Abordons maintenant les mécanismes de l'évolution. Mais comment la définir ? 

L'évolution est définie comme :
1) [les mécanismes par lesquels les populations varient, les espèces se transforment, naissent, s’adaptent](Évolution q2)
2) [le développement des individus au cours de leur vie](Erreur)
3) [la variation de la taille des populations dans le temps](Erreur)
4) [la reproduction des individus les plus forts](Erreur)


## Évolution q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

L'**évolution** est le processus par lequel des espèces ou groupes d'espèces apparaissent, se diversifient, s'éteignent ou se maintiennent.

Ces modifications s'étudient au niveau des **populations** : ensemble d'individus de la même espèce sur un même territoire.

Allons voir de plus près les différents mécanismes de l'évolution.

La dérive génétique est un mécanisme évolutif qui :

1) [est liée à des variations aléatoires dans les fréquences des allèles](Évolution q3)
2) [favorise systématiquement les individus les mieux adaptés à leur milieu](Erreur1)
3) [conduit dans tous les cas à une augmentation de la diversité génétique](Erreur2)
4) [est plus forte dans les grandes populations](Erreur3)


## Évolution q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La **dérive génétique** est un processus lié au hasard de la reproduction : au fil des générations, des allèles peuvent aléatoirement voir leurs fréquences diminuer ou augmenter. Certains peuvent même carrément disparaître, du fait de la dérive.

Elle se produit de manière plus marquée quand la population est petite et isolée géographiquement (par exemple sur une île).

1. [En savoir plus](!useLLM Explique comment la dérive génétique agit sur une petite population isolée.)
2. [Question suivante](Évolution q3bis)


## Évolution q3bis

L'autre grand mécanisme évolutif est la sélection naturelle.

La sélection naturelle agit principalement :
1) [en favorisant les individus les mieux adaptés à leur environnement](Évolution q4)
2) [en augmentant la taille des populations](Erreur)
3) [en réduisant la variabilité génétique](Erreur)
4) [en introduisant plusieurs nouveaux allèles dans la population d'origine](Erreur)


## Évolution q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 

Quand la **sélection naturelle** est à l'œuvre sur une espèce, on observe qu'un ou plusieurs éléments de l'environnement exercent des contraintes qui aboutissent à des modifications, à des adaptations de cette espèce.

Comment nomme-t-on ces contraintes ?

1) [pression de sélection](Évolution q5)
2) [convergence évolutive](Erreur)
3) [symbiose](Erreur)
4) [camouflage](Erreur)



## Évolution q5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

On peut distinguer deux principaux types de **pressions de sélection** :

* les pressions de sélection exercées par les autres êtres vivants (prédateur, parasite, compétiteur...)
* les pressions de sélection abiotiques, dues à des facteurs non vivants (comme la composition chimique de l'environnement, la température, etc.)

Les conditions extrêmes de vie dans le désert sont un exemple de pression de sélection abiotique : les espèces qui occupent cette niche écologique sont celles qui ont développé par exemple des mécanismes de régulation de la température interne du corps. Elles sont adaptées et sont relativement protégées à la chaleur et à la déshydratation. 

1. [En savoir plus](!useLLM Explique l'hypothèse de la Reine Rouge en évolution.)
2. [Question suivante](Évolution q5bis)

## Évolution q5bis

La spéciation est :
1) [la formation de nouvelles espèces à partir d'ancêtres communs](BiodivEvol 2de fin)
2) [l'extinction des espèces mal adaptées](Erreur)
3) [l'accumulation de mutations neutres, désavantageuses ou avantageuses](Erreur)
4) [l'évolution convergente de traits similaires](Erreur)


## BiodivEvol 2de fin

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo}

La **spéciation** est le phénomène au cours duquel les espèces s'individualisent à partir de populations appartenant à une espèce d'origine. 
Il y a spéciation lorsque deux groupes partageant les mêmes ancêtres ne sont plus interféconds, et prennent ainsi le statut d'espèces biologiques vraies.

Tu as atteint le niveau 2.

On s'arrête ici pour les révisions niveau 2de !

1. [Passer au niveau 1ère spécialité SVT](Biodiv 1spé)
2. [Retour au message initial]()




## Biodiv 1spé

EN TRAVAUX :construction_worker_woman: 
Reviens plus tard ! 


## Erreur

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav

@{erreur}

@{encourage}


## Erreur1

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav

C'est une erreur :anguished: 
Cherche encore.

## Erreur2

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav

Eh non :cry:
Recommence. 

## Erreur3

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav

Pas de chance :-1: 
Essaye encore !

## Erreur échelle

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
@{erreur}

Il faudrait revoir les niveaux d'organisation du vivant !
![échelles du vivant](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/echelle-du-monde-vivant.jpg)

@{encourage}


