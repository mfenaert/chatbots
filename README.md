# Chatbots

## Description

Chatbots à vocation pédagogique, créés par Mélanie Fenaert avec l'outil ChatMD de Cédric Eyssette

- Bobot : pour une formation sur les chatbots pédagogiques, Bobot est le compagnon du parcours Magistère
https://chatmd.forge.apps.education.fr/#https://mfenaert.forge.apps.education.fr/chatbots/BoBot.md

- ViTa : une assistante de révision du programme de 2de et 1ère en SVT. 
Le contenu de niveau 2de est pré-construit, le contenu de niveau 1ère spé SVT sera construit avec les élèves.
https://chatmd.forge.apps.education.fr/#https://mfenaert.forge.apps.education.fr/chatbots/ViTa.md

- ViTa : Le chatbot est progressivement amendé au fil de l'année avec des propositions des élèves (QCM, prompts...) et de l'enseignante.
Version de la Toussaint 2024 : https://chatmd.forge.apps.education.fr/#https://mfenaert.forge.apps.education.fr/chatbots/ViTa-saveToussaint.md

- Article de présentation de ViTa sur le site SVT Versailles : https://svt.ac-versailles.fr/spip.php?article1355

## Licence

CC BY NC SA 

