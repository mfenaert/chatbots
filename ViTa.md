---
style: h1{font-family:Papyrus, sans-serif;color:#347082;text-decoration:bold}li{font-family:Verdana, "DejaVu Sans", sans-serif}p{font-family:Verdana, "DejaVu Sans", sans-serif}footer{font-family:Papyrus, sans-serif}aside{font-size:0.5em}audio{visibility:hidden}main{background:linear-gradient(90deg, rgb(239,247,242) 0%, rgb(237,249,240) 15.2057%, rgb(235,248,239) 26.297%, rgb(235,248,239) 28.9803%, rgb(231,249,237) 47.585%, rgb(230,250,236) 48.6583%, rgb(228,249,236) 57.6029%, rgb(227,250,234) 65.4741%, rgb(222,250,234) 72.093%, rgb(219,248,230) 79.4275%, rgb(216,248,229) 86.2254%, rgb(213,249,228) 89.4454%, rgb(210,249,226) 94.2755%, rgb(209,248,225) 96.6011%, rgb(208,247,224) 100%)}.bot-message ul.messageOptions li>a:hover{background-color:#B6CE8A;border-color:#43481D}.user-message{background-color:#447C25}
contenuDynamique: true
variables:
    bravo: "Bravo ! /// Tout à fait ! /// Bonne réponse ! /// C'est exact ! /// C'est une bonne réponse !"
    erreur: "C'est une erreur :-1:/// Eh non :anguished:/// Pas de chance :laughing:/// C'est inexact :anguished: /// Ce n'est pas la bonne réponse :cry: "
    encourage: " Cherche encore. /// Recommence. /// Essaye encore ! /// Retente ta chance ! /// Réfléchis encore."
avatar: https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa_-_rond.png
favicon: https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa_-_rond2.png
clavier: false
tags: chatbot, ViTa, SVT, 2de, 1èreSpé
obfuscate: true
gestionGrosMots: true
rechercheContenu: false
footer: CC BY NC SA Mélanie Fenaert 2024 - Créé avec ChatMD, un outil libre et gratuit de Cédric Eyssette
useLLM:
    url: https://api.cohere.com/v1/chat
    askAPIkey: true
    informations: "https://codimd.apps.education.fr/H5ALGWxfQYai0c2T9FZb6Q/download"
    separator: auto
    postprompt: "Réponds en français. Arrête-toi à la fin d'une phrase avant d'atteindre les 150 tokens."
    model: command-r
    maxTopElements: 5
    maxTokens: 150
    
---

# ViTa
`@KEYBOARD = true`

Bonjour, je suis **ViTa** !

![Avatar de ViTa : pixel art d'une enseignante avec une blouse et un stylo devant un tableau à craie et un microscope](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa_-_rond.png "Image créée avec Copilot Designer")

Je suis ton assistante de révision en Sciences de la vie et de la Terre.

Je suis spécialisée dans les niveaux 2de et 1ère spécialité SVT.

**Choisis un thème** ci-dessous, pour répondre à des questions dans le niveau voulu. 

Pour avoir plus d'explications sur certaines notions, choisis **En savoir plus**, mais garde un regard critique sur mes réponses ! :wink: 

Quel sujet souhaites-tu réviser ? 

1. [La biodiversité et son évolution](Biodiversité)
2. [Le corps humain et la santé](Santé)
3. [La génétique](Génétique)
4. [La planète Terre](Terre)

## Terre

Cette partie s'intéresse à notre planète du point de vue géologique : sa structure interne, les manifestations en surface de son activité interne, les phénomènes externes comme l'érosion, la formation des sols...

Choisis ton niveau.

1. [Niveau collège-2de](Terre 2de)
2. [Niveau 1ère spécialité SVT](Terre 1spé)
3. [Retour au message initial]()

## Terre 2de

Dans ce quiz nous allons revoir des notions de collège et de 2de qui seront utiles pour la classe de 1ère.

1. [Démarrer le quiz](Terre q1)

## Terre q1

Notre planète est une planète rocheuse. Elle est constituée de plusieurs couches concentriques.

La couche la plus externe de la Terre est :
1) [la croûte terrestre](Terre q1bis)
2) [le manteau](Erreur)
3) [le noyau externe](Erreur)
4) [le noyau interne](Erreur)

## Terre q1bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Voici un schéma simplifié de la structure interne de la Terre.

![Structure interne Terre niveau collège](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/structureterre-college.PNG)
<aside>Source : SVT Dijon modifié</aside>

1. [Question suivante](Terre q2)

## Terre q2

Le manteau terrestre est principalement composé de :
1) [roches solides](Terre q3)
2) [glace](Erreur)
3) [métaux liquides](Erreur)
4) [magma](Erreur)


## Terre q3
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Le manteau est bien constitué de **roches solides**. 

1. [En savoir plus](!useLLM Explique quelle roche constitue le manteau terrestre et pourquoi elle reste solide dans le manteau.)
2. [Question suivante](Terre q3bis)

## Terre q3bis

Au-delà de la distinction croûte/manteau/noyau, on parle aussi de la lithosphère. De quoi est-elle constituée ?

1) [la croûte terrestre et la partie supérieure du manteau supérieur](Terre q4)
2) [la croûte terrestre et la totalité du manteau](Erreur)
3) [le manteau inférieur et le noyau externe](Erreur)
4) [le noyau externe et le noyau interne](Erreur)


## Terre q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

La **lithosphère** est bien constituée de la croûte terrestre associée à la partie supérieure du manteau supérieur (le manteau lithosphérique).

La lithosphère terrestre est découpée en grandes plaques.

![Plaques tectoniques](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/1024px-Tectonic_plates-fr.png)
<aside>Source : USGS, Public domain, via Wikimedia Commons</aside>

Une **plaque lithosphérique ou tectonique** est une zone stable de la surface de la Terre délimitée par des zones de forte activité géologique.

Les plaques tectoniques sont en mouvement à la surface de la Terre. Elles se déplacent sur :
1) [l'asthénosphère](Terre q4bis)
2) [la croûte terrestre](Erreur)
3) [le noyau externe](Erreur)
4) [la lithosphère](Erreur)

## Terre q4bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Sous la lithosphère, bien que solide, le manteau supérieur a une rigidité moindre, une plus grande capacité de déformation : on parle d'**asthénosphère** ou de manteau asthénosphérique.

2. [Question suivante](Terre q5)

## Terre q5

La limite où deux plaques tectoniques se rencontrent est appelée une frontière de plaques, ou encore une marge active. Selon les frontières, on peut y observer :
1) [un important volcanisme](volcanisme)
2) [de nombreux tremblements de terre](tremblements)
3) [des failles](failles)
4) [de profonds fossés océaniques](fosses)

## volcanisme
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Les frontières de plaques sont en effet marquées par un important volcanisme, continental comme par exemple dans la Cordillère des Andes, ou sous-marin comme au milieu de l'Atlantique.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Terre q6)

## tremblements
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Les frontières de plaque se caractérisent par une forte activité sismique.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Terre q6)

## failles
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Les failles de différents types (normales, inverses, décrochantes, transformantes) sont présentes au niveau des frontières de plaques.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Terre q5bis)

## fosses
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Dans certaines zones, les limites de plaques sont en effet bordées de profondes fosses océaniques : c'est par exemple le cas tout autour de l'océan Pacifique.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Terre q5bis)

## Terre q5bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

Toutes les propositions étaient correctes ! 

Aux frontières de plaques, on observe une importante **activité sismique et volcanique**, mais aussi des **failles** et selon la zone des **fosses océaniques**.

Tu as atteint le niveau 1.

1. [Passer au niveau 2](Terre q6)
2. [Retour au message initial]()

## Terre q6

L'activité interne de la Terre façonne depuis des milliards d'années les reliefs de notre planète.

Mais il existe aussi des phénomènes se produisant uniquement à la surface, comme l'érosion.

L'érosion est le processus par lequel :
1) [les roches et le sol sont usés par l'eau, le vent et la glace](Terre q6bis)
2) [les montagnes se forment](Erreur)
3) [les volcans émettent de la lave](Erreur)
4) [les plaques tectoniques se déplacent](Erreur)

## Terre q6bis
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

L’**érosion** est le processus de dégradation et de transformation du relief, et donc des sols, roches, berges et littoraux qui est causé **par tout agent externe** (donc autre que la tectonique) tel que l'eau, le vent, la glace.

1. [En savoir plus](!useLLM Donne des exemples d'actions de l'érosion à différentes échelles de temps : minute, heures, années, millions d'années.)
2. [Question suivante](Terre q7)

## Terre q7

Quelle est la composition d'un sol ?
1) [minéraux, matière organique, air et eau](Terre q8)
2) [roches solides uniquement](Erreur)
3) [eau et minéraux uniquement](Erreur)
4) [gaz uniquement](Erreur)

## Terre q8
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Un sol comporte de la matière minérale, de la matière organique, de l'eau et de l'air. 

Il est structuré en différentes couches nommées horizons. Voici par exemple la structure d'un sol forestier : 
![Structure d'un sol](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/sol.PNG)
<aside>Source : SVT Dijon</aside>


La matière organique dans le sol provient principalement :
1) [de la décomposition des plantes et des animaux](Terre q8bis)
2) [de la décomposition des roches](Erreur)
3) [de l'érosion des roches par le vent, l'eau et la glace](Erreur)
4) [des pluies acides](Erreur)

## Terre q8bis
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Les matières organiques des sols forment un ensemble hétérogène constitué de débris végétaux et animaux en cours de décomposition.  

La fraction organique du sol représente un faible pourcentage, de 1% à quelques % dans les sols cultivés.

1. [En savoir plus](!useLLM Explique au niveau expert le phénomène de décomposition de la matière organique par différents processus : chimique, bactéries, champignons.)
2. [Question suivante](Terre q9)

## Terre q9

Qu'est-ce qu'un agrosystème ?

1) [un écosystème modifié par l'être humain pour l'agriculture](Terre q9bis)
2) [un écosystème naturel sans intervention humaine](Erreur)
3) [un système de gestion de l'eau](Erreur)
4) [une zone de protection de la faune](Erreur)

## Terre q9bis
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav
@{bravo}

Un agrosystème est un **écosystème cultivé** : un écosystème, terrestre ou aquatique, modifié par l’humain afin d'exploiter une part de la matière organique qu'il produit, généralement à des fins alimentaires. Ex : rizière, culture de céréales, élevage de poissons...

Mais on trouve aussi des agrosystèmes dont le but est de cultiver des produits non alimentaires, comme les biocarburants. 

1. [Question suivante](Terre q10)

## Terre q10

Quelle activité humaine  peut favoriser l'érosion et l'appauvrissement des sols ?
1) [la déforestation](Terre q10bis)
2) [la plantation d'arbres](Erreur)
3) [la construction de terrasses agricoles](Erreur)
4) [la création de zones naturelles protégées](Erreur)

## Terre q10 bis

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav
@{bravo}

La **déforestation** favorise l'érosion des sols en éliminant les arbres dont les racines stabilisent et retiennent le sol. Sans cette protection, le sol devient plus vulnérable à l'action de l'eau et du vent, ce qui l'appauvrit de ses éléments fertiles.

1. [En savoir plus](!useLLM Explique comment la déforestation peut aboutir à créer des sols incultivables.)
2. [Terminer le test](Terre Fin)

## Terre Fin

Félicitations, tu as atteint le niveau 2 de ces révisions de collège et 2de sur la géologie de notre planète !

1. [Passer au niveau 1ère spécialité SVT](Terre 1spé)
2. [Retour au message initial]()


## Terre 1spé
EN TRAVAUX :construction_worker_woman: 
Reviens plus tard ! 










## Santé

Cette partie est consacrée à la **santé humaine**, qui recouvre de nombreux champs : reproduction, microbiote, maladies infectieuses, défenses de l'organisme, fonctionnement du cerveau, régulation hormonale...

Choisis ton niveau.

1. [Niveau collège-2de](Santé 2de)
2. [Niveau 1ère spécialité SVT](Santé 1spé)
3. [Retour au message initial]()


## Santé 2de

Dans ce quiz de 10 questions, tu vas réviser des notions de base du programme de 2de sur le corps humain et la santé.

1. [Démarrer le quiz](Santé q1)

## Santé q1

Quelle proposition correspond à des niveaux biologiques correctement ordonnés, du plus grand au plus petit ?
1) [organisme > organe > tissu > cellule > molécule](Santé q2)
2) [organisme > tissu > organe > cellule > molécule](Erreur)
3) [molécule > cellule > tissu > organe > organisme](Erreur)
4) [organisme > tissu > organe > molécule > cellule](Erreur)

## Santé q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Tu as correctement ordonné les différentes échelles au sein du monde vivant. Le schéma ci-dessous te permet de les visualiser, avec des détails du point de vue moléculaire.
![échelles du vivant](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/echelle-du-monde-vivant.jpg)

1. [Question suivante](Santé q2bis)

## Santé q2bis
Continuons !

La mise en place et la mise en fonctionnement de l'appareil sexuel se réalise :
1) [de la fécondation à la puberté](Santé q3)
2) [à partir de la naissance](Erreur)
3) [pendant l'adolescence](Erreur)
4) [uniquement chez les filles](Erreur)


## Santé q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 

Cette mise en place des organes reproducteurs et leur mise en route se réalise sous l'influence d'**hormones**. Ces molécules sont fabriquées par des glandes et sécrétées dans le sang. Comment agissent-elles sur leurs organes-cibles ?

1) [Elles les stimulent électriquement](Erreur)
2) [Elles se fixent à des récepteurs spécifiques sur la membrane des cellules-cibles](Santé q4)
3) [Elles activent la production d'enzymes spécifiques à la surface des organes-cibles](Erreur)
4) [Elles activent la division cellulaire dans les organes cibles](Erreur)

## Santé q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Ce schéma résume les différentes étapes de la communication hormonale.
![schema communication homonale](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/schema_comm_hormonale_-_dijon_modifi%C3%A9.PNG "Source : SVT Dijon, modifié")

<aside>Source : SVT Dijon, modifié</aside>


1. [Question suivante](Santé q4bis)

## Santé q4bis
Continuons !

Le plaisir chez l'humain repose notamment sur :
1) [l'activation de certains neurones dans le cerveau](Santé q5)
2) [l'activation des muscles squelettiques](Erreur)
3) [la libération d'insuline par le pancréas](Erreur)
4) [l'activation de la moelle épinière](Erreur)

## Santé q5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Le **circuit de la récompense** est constitué de différentes zones du cerveau, elles-mêmes constituées de neurones interconnectés, qui s'activent lors d'une stimulation agréable et libèrent de la dopamine. La libération de cette molécule dans le cerveau provoque une sensation de plaisir pour l'individu.

On peut retenir de cet exemple que **des mécanismes moléculaires et cellulaires ont un impact à l'échelle des organes et de l'organisme**.

Changeons de sujet...

Chlamydia, Papillomavirus HPV, VIH... Ces micro-organismes ont un point commun : lequel ?
1) [Ils se transmettent tous par voie sexuelle](Santé q6)
2) [Ce sont tous des virus](Erreur1)
3) [Ils se transmettent tous par voie aérienne](Erreur2)
4) [Ce sont des micro-organismes "géants" d'environ 0,1 mm](Erreur3)

## Santé q6
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo} 

Ces micro-organismes se transmettent tous par voie sexuelle, certains comme le VIH se transmettent aussi par voie sanguine. Ils sont à l'origine des **IST** : infections sexuellement transmissibles.

Bravo, tu as atteint le niveau 1.

1. [En savoir plus sur la prévention des IST](!useLLM Explique quelles sont les mesures de prévention des infections sexuellement transmissibles.)
2. [Passer au niveau 2](Santé q6bis)
3. [Retour au message initial]()

## Santé q6bis
C'est parti pour 5 questions supplémentaires !

En parlant de micro-organismes, tu as dû entendre parler du microbiote. De quoi s'agit-il ?

1) [l'ensemble des microorganismes vivant dans et sur le corps humain](Santé q7)
2) [les cellules immunitaires du corps humain](Erreur1)
3) [les hormones circulant dans le sang](Erreur2)
4) [les toxines et les molécules de défense produites par les cellules humaines](Erreur3)

## Santé q7
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Le **microbiote** est bien l'ensemble des microorganismes vivant dans et sur le corps humain. Il colonise la peau et les muqueuses comme notre tube digestif ou les muqueuses génitales.

Comment nomme-t-on notre relation avec notre microbiote ?
1) [Symbiose](Santé q8)
2) [Parasitisme](Erreur1)
3) [Prédation](Erreur2)
4) [Commensalisme](Erreur3)

## Santé q8
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

C'est bien une **symbiose** : une association à bénéfice réciproque.

Tous les types de relations proposés à la question précédente existent dans le monde vivant. 

1. [En savoir plus sur la symbiose](!useLLM Explique au niveau scientifique expert ce qu'est une relation symbiotique et donne un exemple.)
2. [En savoir plus sur la prédation](!useLLM Explique au niveau scientifique expert ce qu'est une relation prédateur-proie et donne un exemple.)
3. [En savoir plus sur le commensalisme](!useLLM Explique au niveau scientifique expert ce qu'est le commensalisme et donne un exemple.)
4. [En savoir plus sur le parasitisme](!useLLM Explique au niveau scientifique expert ce qu'est le parasitisme et donne un exemple.)
5. [Question suivante](Santé q8bis)



## Santé q8bis

Les agents pathogènes peuvent être :
1) [des bactéries, des virus, des champignons ou des eucaryotes parasites](Santé q9)
2) [des virus ou des bactéries uniquement](Erreur)
3) [des bactéries uniquement](Erreur)
4) [des bactéries et des eucaryotes parasites uniquement](Erreur)


## Santé q9
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les agents pathogènes peuvent appartenir à **tous les types de micro-organismes**. Mais tous les micro-organismes ne sont pas pathogènes bien sûr ! Certains sont bénéfiques, comme ceux du microbiote.

Les agents pathogènes vivent aux dépens d’un autre organisme, tout en lui portant préjudice (les symptômes). Cet organisme devient leur milieu de vie.

Comment nomme-t-on cet organisme ?

1) [hôte](Santé q10)
2) [donneur](Erreur)
3) [receveur](Erreur)
4) [vecteur](Erreur)

## Santé q10
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les agents pathogènes vivent aux dépens d’un autre organisme, appelé **hôte**.

La propagation du pathogène se fait par changement d’hôte. Il se fait soit par contact entre hôtes, soit par le milieu ambiant (air, eau), soit par un **vecteur biologique** qui est alors l’agent transmetteur indispensable du pathogène.

1. [En savoir plus](!useLLM Explique le mode de propagation de la maladie de Lyme à un niveau scientifique expert en utilisant les mots : pathogène, hôte, vecteur, cycle. Sois précis mais bref.)
2. [Passer à la dernière question du quiz](Santé q10bis)


## Santé q10bis

Le VIH, la grippe, sont des exemples de maladies à transmission directe.
Le paludisme, la maladie de Lyme, sont des exemples de maladies vectorielles.

Dans tous les cas, la propagation des pathogènes peut être plus ou moins rapide et provoquer une **épidémie**. 

Quel(s) comportement(s) permettent de limiter la propagation d'un pathogène ?


1) [la vaccination](vaccination)
2) [les mesures d'hygiène](hygiène)
3) [les mesures de protection](protection)
4) [les incantations](Erreur)


## vaccination
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La **vaccination** est une mesure préventive permettant d'empêcher le développement et les conséquences parfois mortelles de certaines infections comme la rougeole, la grippe, l'infection au HPV (papillomavirus) ou encore les hépatites A et B.

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Santé Fin)

## hygiène
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les **mesures d'hygiène** sont essentielles pour limiter les risques d'infection et de propagation des pathogènes : se laver les mains régulièrement, se moucher dans un mouchoir jetable (et le jeter à la poubelle !), éternuer dans son coude...  

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Santé Fin)

## protection
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les **mesures de protection** permettent de repousser ou éliminer les pathogènes ou leurs agents de transport : porter des vêtements longs dans les zones à risque (maladies vectorielles), utiliser un produit répulsif, éliminer les eaux stagnantes où se reproduisent les moustiques, porter un masque, porter un préservatif lors d'un rapport sexuel...

Il y avait d'autres bonnes réponses, les as-tu trouvées ?

Sélectionne une autre bonne réponse ou passe à la suite.

1) [Passer à la suite](Santé Fin)

## Santé Fin
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

Il y avait trois bonnes réponses possibles ! Les comportements permettant de limiter la propagation d'un pathogène sont :
- la **vaccination**, si elle existe pour ce pathogène
- les **mesures d'hygiène** : se laver les mains, éternuer dans son coude...
- les **mesures de protection** : porter des vêtements longs dans les zones à risque (maladies vectorielles), utiliser un produit répulsif, éliminer les eaux stagnantes, porter un masque...

Félicitations, tu as atteint le niveau 2 de ces révisions de 2de sur le corps humain et la santé !

1. [Passer au niveau 1ère spécialité SVT](Santé 1spé)
2. [Retour au message initial]()



## Santé 1spé

EN TRAVAUX :construction_worker_woman: 
Reviens plus tard ! 











## Génétique

La **génétique** est l'étude de la transmission des caractères héréditaires chez les êtres vivants. 

Elle vise à déterminer les modes de transmission et à documenter les variations dans les gènes entre les individus d'une même espèce.

Choisis ton niveau.

1. [Niveau collège-2de](Gen 2de)
2. [Niveau 1ère spécialité SVT](Gen 1spé)
3. [Retour au message initial]()


## Gen 2de

C'est parti pour un petit quiz de génétique !

Les nucléotides sont des molécules constitutives de l'ADN.

Comment qualifie-t-on la structure de l'ADN ?

1) [simple brin](Erreur)
2) [double-hélice](suite gen1)
3) [entortillée](Erreur)
4) [tricaténaire](Erreur)


## suite gen1
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

L'ADN est une molécule constituée de nucléotides associés en deux chaînes, ou brins, enroulées en **double-hélice**.

**Schéma animé de la structure en double hélice de l'ADN** 

![GIF ADN](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/DNA_animation.gif "Source: Wikimédia, domaine public")
<aside>Source: Wikimédia, domaine public</aside>

**Schéma de la structure moléculaire de l'ADN**

![schéma ADN](https://forge.apps.education.fr/mfenaert/chatbots/-/raw/main/Images/ViTa/DNA_structure_and_bases_FR.svg.png "Wikimédia, CC BY SA Dosto")
<aside>Source: Wikimédia, CC BY SA Dosto</aside>

1. [En savoir plus](!useLLM Explique la structure de l'ADN.)
2. [Question suivante](suite gen2)

## suite gen2

Les nucléotides qui se font face sont complémentaires 2 à 2.

Quel est le nucléotide complémentaire de A ?
`@nc = A`

---
Les nucléotides qui se font face sont complémentaires 2 à 2.

Quel est le nucléotide complémentaire de T ?
`@nc = T`

---
Les nucléotides qui se font face sont complémentaires 2 à 2.

Quel est le nucléotide complémentaire de G ?
`@nc = G`

---
Les nucléotides qui se font face sont complémentaires 2 à 2.

Quel est le nucléotide complémentaire de C ?
`@nc = C`

1. [A @ncAnswer=A](question nucléotide)
2. [T @ncAnswer=T](question nucléotide)
3. [G @ncAnswer=G](question nucléotide)
4. [C @ncAnswer=C](question nucléotide)

## question nucléotide

`if @nc==A && @ncAnswer==T`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}
A et T sont bien 2 nucléotides complémentaires.
`endif`

`if @nc==T && @ncAnswer==A`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
A et T sont bien 2 nucléotides complémentaires.
`endif`

`if @nc==C && @ncAnswer==G`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
C et G sont bien 2 nucléotides complémentaires.
`endif`

`if @nc==G && @ncAnswer==C`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
C et G sont bien 2 nucléotides complémentaires.
`endif`

`if @nc==G && !(@ncAnswer==C)`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
C'est une erreur ! Réessaye.
`endif`

`if @nc==C && !(@ncAnswer==G)`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
C'est une erreur ! Réessaye.
`endif`

`if @nc==A && !(@ncAnswer==T)`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
C'est une erreur ! Réessaye.
`endif`

`if @nc==T && !(@ncAnswer==A)`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
C'est une erreur ! Réessaye.
`endif`


1. [Encore des nucléotides](suite gen2)
2. [Question suivante](suite gen2bis)


## suite gen2bis
`@KEYBOARD = true`
Tape en majuscules la séquence complémentaire de celle-ci : 
GATC

!Next: Bonne réponse q1 / Tu n'as pas donné la bonne séquence, essaie encore !


## Bonne réponse q1
- CTAG

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}
La séquence complémentaire de : 
GATC  est bien : 
CTAG

Tu as bien placé un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.

1. [Me poser une question similaire](suite gen2ter)
2. [Passer à la question suivante](suite gen3)


## suite gen2ter
`@KEYBOARD = true`
Tape en majuscules la séquence complémentaire de celle-ci : 
CTAATGT

!Next: GATTACA / Tu n'as pas donné la bonne séquence, essaie encore !

1. [Je ne sais pas / Voir la réponse](GATTACA sais pas)


## GATTACA

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
La séquence complémentaire de : 
CTAATGT  est bien : 
GATTACA

Tu as bien placé un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.

1. [Me poser une question similaire](GATTACA suite)
2. [Passer à la question suivante](suite gen3)


## GATTACA sais pas

La séquence complémentaire de : 
CTAATGT  est : 
GATTACA

Il faut placer un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.

1. [Réessayer](GATTACA suite)
2. [Question suivante](suite gen3)


## GATTACA suite
`@KEYBOARD = true`
Tape en majuscules la séquence complémentaire de celle-ci : 
ATCATGTTG

!Next: TAGTACAAC / Tu n'as pas donné la bonne séquence, essaie encore !

1. [Je ne sais pas / Voir la réponse](TAGTACAAC sais pas)

## TAGTACAAC

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 
La séquence complémentaire de : 
ATCATGTTG  est bien : 
TAGTACAAC

Tu as bien placé un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.


1. [Question suivante](suite gen3)



## TAGTACAAC sais pas

La séquence complémentaire de : 
ATCATGTTG  est : 
TAGTACAAC

Il faut placer un A face à un T, et un G face à un C, pour respecter la complémentarité des nucléotides.

1. [Question suivante](suite gen3)



## suite gen3

L'ADN est constitutif de quel(s) élément(s) de la cellule ? 
Une seule bonne réponse.

1) [Les chromosomes](suite gen4)
2) [La membrane plasmique](Erreur1)
3) [L'enveloppe nucléaire](Erreur2)
4) [Le cytoplasme](Erreur3)

## suite gen4

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Les **chromosomes** sont constitués d'ADN.

La succession des nucléotides le long des chromosomes constitue un ensemble de messages que la cellule peut "lire" : c'est **l'information génétique**.

Combien une cellule humaine (gamètes non compris) contient-elle de chromosomes ?

1) [23 paires](suite gen4bis)
2) [23](Erreur1)
3) [46 paires](Erreur2)
4) [46](suite gen4bis)

## suite gen4bis
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 

Il y avait deux bonnes réponses possibles :wink: 
Une cellule humaine contient **46 chromosomes**, qui correspondent à **23 paires de chromosomes**. On les appelle des paires de chromosomes homologues.

1. [En savoir plus](!useLLM Explique ce que sont des chromosomes homologues.)
2. [Question suivante](suite gen4ter)

## suite gen4ter
`@KEYBOARD = true`
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

Bravo pour ta persévérance : tu as atteint le niveau 1.

1. [En savoir plus](!useLLM Explique ce qu'est le caryotype d'un individu.)
2. [Passer au niveau 2](suite gen4-4)
3. [Retour au message initial]()

## suite gen4-4

C'est parti pour 5 questions supplémentaires !

Revenons à la séquence d'ADN... Comment nomme-t-on une séquence de nucléotides permettant de coder un caractère héréditaire ?

1) [Un gène](suite gen5)
2) [Un allèle](Erreur1)
3) [Un texto](Erreur2)
4) [Une mutation](Erreur3)

## suite gen5

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Un **gène** est une séquence d'ADN qui code pour un caractère héréditaire, ou plus précisément pour une protéine responsable d'un caractère héréditaire.

Cette information génétique peut subir des modifications de manière aléatoire :  quel nom donne-t-on à cette modification de la séquence d'ADN ?

1) [Une transmutation](Erreur1)
2) [Une mutation](suite gen6)
3) [Une coquille](Erreur2)
4) [Un allèle](Erreur3)


## suite gen6

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

C'est une **mutation** : un nucléotide (ou plusieurs) peut être remplacé par un autre, ou supprimé, ou ajouté.

1. [En savoir plus](!useLLM Explique ce qu'est une mutation et les conséquences possibles.)
2. [Question suivante](suite gen6bis)

## suite gen6bis

Si cette nouvelle version du gène est transmise aux générations suivantes, comment l'appellera-t-on ?

1) [un allèle du gène](suite gen7)
2) [un gène de l'allèle](Erreur1)
3) [un clone du gène](Erreur2)
4) [un allèle du clone](Erreur3)
5) [l'attaque des clones](clone)


## clone
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
Tu l'as fait exprès n'est-ce pas ? :laughing: 

Allez réessaye.

## suite gen7

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

C'est en effet un nouvel **allèle** du gène qui est apparu dans la population !

Comment nomme-t-on un individu qui possède deux allèles identiques d'un même gène ?

1) [hétérozygote](Erreur2)
2) [clone](Erreur1)
3) [homozygote](suite gen8)
4) [dizygote](Erreur3)

## suite gen8

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Avec deux allèles identiques d'un même gène, un individu est qualifié d'**homozygote** pour ce gène.


Comment nomme-t-on un individu qui possède deux allèles différents d'un même gène ?

1) [hétérozygote](suite gen9)
2) [clone](Erreur1)
3) [homozygote](Erreur2)
4) [monozygote](Erreur3)


## suite gen9

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo} 

Si les deux allèles sont différents, il est qualifié d'**hétérozygote** pour ce gène.

Tu as atteint le niveau 2.

On s'arrête ici pour les révisions niveau 2de !

1. [Passer au niveau 1ère spécialité SVT](Gen 1spé)
2. [Retour au message initial]()

---

## Gen 1spé

EN TRAVAUX :construction_worker_woman: 
Reviens plus tard ! 

---

## Biodiversité

La **biodiversité** désigne l’ensemble des êtres vivants ainsi que les écosystèmes dans lesquels ils vivent. Ce terme comprend également les interactions des espèces entre elles et avec leurs milieux. 

La biodiversité a changé depuis l'apparition des premières formes de vie sur Terre. Elle **évolue** et continue de changer actuellement.

Choisis ton niveau.

1. [Niveau collège-2de](Biodiv 2de)
2. [Niveau 1ère spécialité SVT](Biodiv 1spé)
3. [Retour au message initial]()



## Biodiv 2de

Le niveau 1 du quiz explore les différentes **échelles de la biodiversité**, le niveau 2 te questionne sur les **mécanismes de l'évolution**.

1. [C'est parti !](Biodiv q1)
2. [Retour au message initial]()

## Biodiv q1

La plus grande échelle à laquelle on définit la biodiversité est la diversité...

1) [des écosystèmes](Biodiv q2)
2) [génétique](Erreur1)
3) [des espèces](Erreur2)
4) [intra-spécifique](Erreur3)

## Biodiv q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

Il existe une grande **diversité d'écosystèmes** à la surface de notre planète : la forêt tempérée, la forêt tropicale, le désert, le littoral, une grotte, l'écosystème urbain, les fonds marins... 

Un écosystème n'est pas forcément très grand : même une flaque d'eau, une poignée de terre ou notre intestin peuvent être considérés comme des écosystèmes, avec une biodiversité très riche !

1. [En savoir plus](!useLLM Explique ce qu'est un écosystème et donne quelques exemples d'écosystèmes.)
2. [Question suivante](Biodiv q2bis)

## Biodiv q2bis

Au sein d'un écosystème, il existe de nombreuses espèces d'êtres vivants en interaction. C'est la biodiversité...

1) [extra-spécifique](Erreur2)
2) [génétique](Erreur1)
3) [spécifique](Biodiv q3)
4) [intra-spécifique](Erreur3)

## Biodiv q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La biodiversité des espèces est aussi appelée la **biodiversité spécifique**.

Mais comment sait-on que deux individus sont de la même espèce ?

1) [ils se ressemblent toujours et peuvent avoir des descendants](Erreur3)
2) [ils vivent dans le même milieu](Erreur1)
3) [ils partagent de nombreux points communs : mode de vie, nourriture, territoire...](Erreur2)
4) [ils peuvent se ressembler, se reproduire ensemble et avoir une descendance fertile](Biodiv q4)


## Biodiv q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

C'est en effet la meilleure réponse !

Deux individus de la même espèce ne partagent pas nécessairement le même milieu ni le même mode de vie. Les critères les plus couramment retenus par les scientifiques sont :
- la ressemblance
- l'interfécondité
- la descendance viable et fertile

Parfois il n'est pas possible de vérifier tous ces critères (espèces fossiles, fort dimorphisme sexuel...), on ne retient que ceux qui sont vérifiables.

1. [En savoir plus](!useLLM Explique pourquoi il est difficile de donner une définition de l'espèce valable dans toutes les situations.)
2. [Question suivante](Biodiv q4bis)

## Biodiv q4bis

Au sein d'une espèce, on observe aussi une diversité entre les individus. On l'appelle biodiversité...

1) [allélique](Biodiv q5)
2) [génétique](Biodiv q5)
3) [écosystémique](Erreur)
4) [intra-spécifique](Biodiv q5)



## Biodiv q5

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

Il y avait trois bonnes réponses à cette question ! 

La biodiversité au sein d'une espèce peut être aussi nommée : 
- biodiversité intra-spécifique
- biodiversité génétique
- biodiversité allélique

1. [En savoir plus](!useLLM Explique comment deux individus d'une même espèce peuvent avoir les mêmes gènes mais être différents.)
2. [Question suivante](Biodiv q5bis)

## Biodiv q5bis

Qu'est-ce qui permet l'existence de plusieurs allèles d'un même gène, engendrant ainsi la biodiversité génétique ?

1) [l'existence des mutations](Évolution q1)
2) [la stabilité de la molécule d'ADN](Erreur)
3) [l'alternance des saisons](Erreur)
4) [la perméabilité de la membrane plasmique](Erreur)

 

## Évolution q1

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav


Bravo ! Tu as atteint le niveau 1.

En effet, au sein de chaque espèce, il existe une diversité génétique due à la présence d'allèles différents d'un même gène, issus de **mutations** survenues au fil des générations. 

1. [En savoir plus](!useLLM Explique les différents types de mutation : par délétion, substitution et insertion.)
2. [Passer au niveau 2](Évolution q1bis)
3. [Retour au message initial]()

## Évolution q1bis

C'est parti pour 5 questions supplémentaires !

Abordons maintenant les mécanismes de l'évolution. Mais comment la définir ? 

L'évolution est définie comme :
1) [les mécanismes par lesquels les populations varient, les espèces se transforment, naissent, s’adaptent](Évolution q2)
2) [le développement des individus au cours de leur vie](Erreur)
3) [la variation de la taille des populations dans le temps](Erreur)
4) [la reproduction des individus les plus forts](Erreur)


## Évolution q2
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

L'**évolution** est le processus par lequel des espèces ou groupes d'espèces apparaissent, se diversifient, s'éteignent ou se maintiennent.

Ces modifications s'étudient au niveau des **populations** : ensemble d'individus de la même espèce sur un même territoire.

Allons voir de plus près les différents mécanismes de l'évolution.

La dérive génétique est un mécanisme évolutif qui :

1) [est liée à des variations aléatoires dans les fréquences des allèles](Évolution q3)
2) [favorise systématiquement les individus les mieux adaptés à leur milieu](Erreur1)
3) [conduit dans tous les cas à une augmentation de la diversité génétique](Erreur2)
4) [est plus forte dans les grandes populations](Erreur3)


## Évolution q3
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

La **dérive génétique** est un processus lié au hasard de la reproduction : au fil des générations, des allèles peuvent aléatoirement voir leurs fréquences diminuer ou augmenter. Certains peuvent même carrément disparaître, du fait de la dérive.

Elle se produit de manière plus marquée quand la population est petite et isolée géographiquement (par exemple sur une île).

1. [En savoir plus](!useLLM Explique comment la dérive génétique agit sur une petite population isolée.)
2. [Question suivante](Évolution q3bis)


## Évolution q3bis

L'autre grand mécanisme évolutif est la sélection naturelle.

La sélection naturelle agit principalement :
1) [en favorisant les individus les mieux adaptés à leur environnement](Évolution q4)
2) [en augmentant la taille des populations](Erreur)
3) [en réduisant la variabilité génétique](Erreur)
4) [en introduisant plusieurs nouveaux allèles dans la population d'origine](Erreur)


## Évolution q4
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo} 

Quand la **sélection naturelle** est à l'œuvre sur une espèce, on observe qu'un ou plusieurs éléments de l'environnement exercent des contraintes qui aboutissent à des modifications, à des adaptations de cette espèce.

Comment nomme-t-on ces contraintes ?

1) [pression de sélection](Évolution q5)
2) [convergence évolutive](Erreur)
3) [symbiose](Erreur)
4) [camouflage](Erreur)



## Évolution q5
!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/615100__mlaudio__magic_game_win_success_2.wav

@{bravo}

On peut distinguer deux principaux types de **pressions de sélection** :

* les pressions de sélection exercées par les autres êtres vivants (prédateur, parasite, compétiteur...)
* les pressions de sélection abiotiques, dues à des facteurs non vivants (comme la composition chimique de l'environnement, la température, etc.)

Les conditions extrêmes de vie dans le désert sont un exemple de pression de sélection abiotique : les espèces qui occupent cette niche écologique sont celles qui ont développé par exemple des mécanismes de régulation de la température interne du corps. Elles sont adaptées et sont relativement protégées à la chaleur et à la déshydratation. 

1. [En savoir plus](!useLLM Explique l'hypothèse de la Reine Rouge en évolution.)
2. [Question suivante](Évolution q5bis)

## Évolution q5bis

La spéciation est :
1) [la formation de nouvelles espèces à partir d'ancêtres communs](BiodivEvol 2de fin)
2) [l'extinction des espèces mal adaptées](Erreur)
3) [l'accumulation de mutations neutres, désavantageuses ou avantageuses](Erreur)
4) [l'évolution convergente de traits similaires](Erreur)


## BiodivEvol 2de fin

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/625714__sonically_sound__achievement.wav

@{bravo}

La **spéciation** est le phénomène au cours duquel les espèces s'individualisent à partir de populations appartenant à une espèce d'origine. 
Il y a spéciation lorsque deux groupes partageant les mêmes ancêtres ne sont plus interféconds, et prennent ainsi le statut d'espèces biologiques vraies.

Tu as atteint le niveau 2.

On s'arrête ici pour les révisions niveau 2de !

1. [Passer au niveau 1ère spécialité SVT](Biodiv 1spé)
2. [Retour au message initial]()




## Biodiv 1spé

EN TRAVAUX :construction_worker_woman: 
Reviens plus tard ! 











## Erreur

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav
@{erreur}
@{encourage}

## Erreur1

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav

C'est une erreur :anguished: 
Cherche encore.

## Erreur2

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav

Eh non :cry:
Recommence. 

## Erreur3

!Audio:https://forge.apps.education.fr/mfenaert/chatbots/-/raw/32da32e85b01e7a067d24e3b368e501ddecdd519/Sons/362206__taranp__horn_fail_wahwah_1.wav

Pas de chance :-1: 
Essaye encore !