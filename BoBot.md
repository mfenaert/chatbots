---
clavier: true
obfuscate: true
tags : chatbot, BoBot, formation
gestionGrosMots: true
avatar: https://mfenaert.forge.apps.education.fr/chatbots/Images/bots-copie-10.png
favicon: https://mfenaert.forge.apps.education.fr/chatbots/Images/bots-copie-10.png
footer: CC BY NC SA Mélanie Fenaert 2024 - Créé avec ChatMD, un outil libre et gratuit de Cédric Eyssette
rechercheContenu: false
style: audio{visibility:hidden}
---

# BoBot

Bonjour, je suis BoBot !
Je suis le chatbot qui te guide sur ce parcours de formation "Enseigner avec les chatbots avec ou sans IA".

Que souhaites-tu découvrir ?

1. [Les objectifs de la formation](Objectifs)
2. [Le planning de la formation](Planning)
3. [Le contenu du parcours m@gistère](Contenu)
4. [La bannière de la formation](Bannière)
5. [Dis-m'en plus sur toi petit robot !](BoBot)
6. [Je sais déjà tout, pose-moi des questions](Questions)

## Bannière
- bannière
- illustration
- image

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/256543__debsound__r2d2-astro-droid.wav

Voici la bannière de la formation : 

![Bannière Enseigner avec les chatbots avec ou sans IA](https://forge.apps.education.fr/mfenaert/fo-chatbots/-/raw/main/Images/chatbots-banniere.png)

Jolie hein !? L'illustration est de Macrovector, sur Freepik.

1. [Retour au message initial]()

## Objectifs
- objectifs
- usages
- but

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/256543__debsound__r2d2-astro-droid.wav

Différents usages pédagogiques des agents conversationnels sont abordés au cours de la formation :
- l'usage professionnel des chatbots reposant sur l'IA comme assistant de l'enseignant
- la création de chatbots préprogrammés ou avec IA pour les apprentissages des élèves
- la création d'un chatbot par les élèves

Que veux-tu découvrir maintenant ?

1. [Le planning de la formation](Planning)
2. [Le contenu du parcours m@gistère](Contenu)
3. [Retour au message initial]()

## Planning
- planning
- emploi du temps
- modalités
- modalité

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/256543__debsound__r2d2-astro-droid.wav

Cette formation est **hybride** : elle comprend des temps en présence, en classe virtuelle et du travail à distance associé au parcours m@gistère. Au total, la formation dure 15h étalées sur 3 à 5 semaines.

Les temps de la formation sont : 
- un présentiel de 6h
- une classe virtuelle de 2h
- un travail asynchrone de 3h
- un dernier présentiel de 4h 

![Planning de la formation](https://forge.apps.education.fr/mfenaert/fo-chatbots/-/raw/main/Images/chatbots-planning.png)

Que désires-tu découvrir maintenant ?

1. [Les objectifs de la formation](Objectifs)
2. [Le contenu du parcours m@gistère](Contenu)
3. [Retour au message initial]()


## Contenu
- contenu
- magistère
- m@gistère
- parcours

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/256543__debsound__r2d2-astro-droid.wav

> Le parcours m@gistère comporte plusieurs pages. Laquelle souhaites-tu explorer ?

1. [Accueil & Forum](Accueil)
2. [En amont de la formation](Amont)
3. [Chatbots : des bases à l'intégration pédagogique](Chatbots)
4. [Ressources à consulter](Ressources)
5. [Développer son chatbot simple](Développer)
6. [Finalisation de votre projet](Projet)
7. [Conclusion](Conclusion)
8. [Retour au message initial]()


## BoBot
- Bobot
- robot
- chatbot

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/256543__debsound__r2d2-astro-droid.wav

![BoBot](https://forge.apps.education.fr/mfenaert/fo-chatbots/-/raw/main/Images/bots_copie_6.png)

Je suis un **chatbot entièrement programmé**, qui utilise aussi la reconnaissance de certains mots pour te permettre de naviguer de manière plus naturelle en tapant une requête.

J'ai été créé avec **ChatMD**, un outil développé par Cédric Eyssette, et disponible sur La Forge des Communs numériques éducatifs (apps.education.fr). Ma structure a été conçue par Mélanie Fenaert avec CodiMD.

Pour mon look, mes créatrices Géraldine Turgis et Mélanie Fenaert ont choisi une série d'illustrations de Macrovector sur Freepik.

![10 BoBots](https://forge.apps.education.fr/mfenaert/fo-chatbots/-/raw/main/Images/bots.png) 

1. [Retour au message initial]()

## Accueil

À la page "Accueil & Forum", tu trouveras : 
- les objectifs de la formation
- le planning et le calendrier
- le forum d'échanges
- la charte : à valider !

1. [Retour au contenu du parcours](Contenu)

## Amont

À la page "En amont de la formation", deux activités sont **à réaliser avant le premier présentiel** : 
- un sondage sur tes attentes pour cette formation
- un test de connaissances (facile -- quoique -- et pour le fun !)

1. [Retour au contenu du parcours](Contenu)

## Chatbots

Cette page contient peu de choses car **l'essentiel est vécu en présentiel**. 

Tu y retrouveras quelques ressources a posteriori, comme un comparatif des outils pour créer des chatbots.

1. [Retour au contenu du parcours](Contenu)

## Ressources

Voilà une page bien fournie ! 

Ces ressources sont variées : 
- un rapport sur les chatbots en éducation (pas de panique, il y a un résumé !)
- l'IA en général et en éducation sous forme d'une vidéo et une interview
- des liens pour aller plus loin...

1. [Retour au contenu du parcours](Contenu)

## Développer

C'est à cette page que se trouve l'accès à la classe virtuelle de la formation. 

Durant ces deux heures, tu apprendras à **développer un chatbot simple**, entièrement programmé, à vocation pédagogique.

1. [Retour au contenu du parcours](Contenu)

## Projet

Cette formation est aussi l'occasion de créer une séance ou séquence pédagogique qui prend appui sur un chatbot, qu'il repose ou pas sur l'IA. 

Cette page contient la **base de données** pour déposer ton projet.

1. [Retour au contenu du parcours](Contenu)

## Conclusion

Toutes les bonnes choses ont une fin ! 

Sur la page de conclusion tu retrouveras : 
- un bilan des apports de la formation
- un questionnaire d'évaluation de la formation
- la possibilité de demander un OpenBadge
- des prolongements vers d'autres formations
- les crédits du parcours

1. [Retour au contenu du parcours](Contenu)

## Questions

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/404759__owlstorm__retro-video-game-sfx-r2d2.wav

On a l'esprit joueur à ce que je vois !

Très bien, c'est parti pour cinq questions...

Q1. Quelle est la durée totale de la formation ?
1. [12h](Erreur1)
2. [15h](Bravo1)
3. [21h](Erreur2)
4. [24h](Erreur3)

## Erreur1

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/362206__taranp__horn_fail_wahwah_1.wav

C'est une erreur :anguished: 
Cherche encore.

## Erreur2

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/362206__taranp__horn_fail_wahwah_1.wav

Eh non :laughing: 
Recommence. 

## Erreur3

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/362206__taranp__horn_fail_wahwah_1.wav

Pas de chance :-1: 
Essaye encore !

## Bravo1
- 15h
- 15H

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/615100__mlaudio__magic_game_win_success_2.wav

Bravo ! La formation dure bien 15h au total.

Q2. À quelle page se situe la charte du parcours à valider dès le début de la formation ?

1) [Finaliser votre projet](Erreur1)
2) [Conclusion](Erreur3)
3) [En amont de la formation](Erreur2)
4) [Accueil & Forum](Bravo2)

## Bravo2
- Accueil
- Accueil et forum
- Accueil & forum
- Accueil & Forum
- Accueil et Forum

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/615100__mlaudio__magic_game_win_success_2.wav

Tout à fait !

La charte pose les droits et devoirs du formateur et des stagiaires, il est important de la valider dès le début de la formation.

Q3. Vérifions si tu as bien fait le test en amont de la formation. Quel est le nom du premier chatbot de l'histoire de l'informatique ?

1. [Jarvis](Erreur3)
2) [Eliza](Bravo3)
3) [Hal](Erreur2)
4) [Géraldine](Erreur1)

## Bravo3
- Eliza
- ELIZA
- eliza

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/615100__mlaudio__magic_game_win_success_2.wav

C'est exact ! ELIZA est le premier chatbot de l'histoire de l'informatique. Créée en 1966, elle avait vocation à jouer le rôle d'une psychanalyste.

Q4. Quel est l'objectif principal de la classe virtuelle de la formation ?
Réponds uniquement via les choix ci-dessous.

1) [Programmer un chatbot pédagogique](Bravo4)
2) [Papoter pendant 2h en buvant du thé](Erreur3)
3) [Programmer une IA forte qui détruira le monde](Erreur1)
4) [Il n'y a pas d'objectifs précis](Erreur2)

## Bravo4

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/615100__mlaudio__magic_game_win_success_2.wav

En effet, on va bien travailler pendant ces deux heures pour programmer un chatbot pédagogique :wink: 

Ultime question...
Q5. Qui a conçu ChatMD, l'outil sur lequel moi, BoBot, j'ai été créé ?
Réponds uniquement via les choix ci-dessous.

1) [Mélanie Fenaert](Erreur2)
2) [Mathieu Latzer](Erreur1)
3) [Cédric Eyssette](Bravo5)
4) [Alan Turing](Erreur3)

## Bravo5

!Audio:https://mfenaert.forge.apps.education.fr/chatbots/Sons/625714__sonically_sound__achievement.wav

C'est une victoire, BRAVO !! :clap: :clap: :clap:

Bon en même temps c'est écrit en bas de cette page, dans le footer.

![BoBot rit vers la droite](https://forge.apps.education.fr/mfenaert/fo-chatbots/-/raw/main/Images/bots_copie_4.png =200x180)

Tu ne gagnes rien si ce n'est la gloire, [l'accès à mon code sur CodiMD](https://codimd.apps.education.fr/DG6uO9GoTjS7Ug1rKnVC0w?both), et le droit de revenir au début.

1. [Retour au message initial]()

